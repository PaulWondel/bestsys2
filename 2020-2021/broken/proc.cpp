#include "proc.h"
#include "instruction.h"
#include "memory.h"

#define RUNNING 'r'
#define SUSPEND 's'
#define TERMINTED 't'

process p_table[MAXPROCESSES];
byte numOfProcesses = 0;

//-------------------------------------------------------------------------

byte getNumOfRunningProgramms()
{

	int numOfRunningProcesses = 0;
	for (byte process = 0; process < MAXPROCESSES; process++)
	{
		switch (p_table[process].state)
		{
		case 'r':
			numOfRunningProcesses++;
			break;
		case 't':
			kill(process);
			break;
		}
	}
	return numOfRunningProcesses;
}

//-------------------------------------------------------------------------

void runPrograms()
{
	byte numOfRunningProcesses = getNumOfRunningProgramms();

	Serial.print(F(">>>> "));

	// devide run time over all running processes
	int startTime = 0;
	double runTime = ceil((double)RUNPROCESSTIME / (double)numOfRunningProcesses);

	for (byte pid = 0; pid < MAXPROCESSES; pid++){
		// Serial.print(F("start program "));
		startTime = millis();
		while (p_table[pid].state == 'r' && (millis() - startTime < runTime))
		{
			if (checkIfStop(pid)){
				kill(pid);
				numOfRunningProcesses--;
			}
		}
	}

	// Serial.print(F(">>>>"));
	// // Schedule through all programs
	// //Serial.println("AARGH");
	// while (numOfRunningProcesses > 0)
	// {
	// 	Serial.print(F("enter loop"));
	// 	//for (byte pid = 0; pid < numOfRunningProcesses+1; pid++){
	// 	for (byte pid = 0; pid < MAXPROCESSES; pid++)
	// 	{
	// 		int startTime = millis();

	// 		while (millis() - startTime < runTime && p_table[pid].state == 'r')
	// 		{
	// 			//Serial.print(F("Spawned with PID: "));
	// 			//Serial.println(pid);
	// 			//while(true && p_table[pid].state == 'r'){
	// 			if (checkIfStop(pid))
	// 			{ // if process is killed, break and lower
	// 				kill(pid);
	// 				numOfRunningProcesses--;
	// 				//Serial.print(F("PID state: "));
	// 				//Serial.println(p_table[pid].state);
	// 			}
	// 		}
	// 	}
	// }
	Serial.println(F(" <<<<"));
}
//-------------------------------------------------------------------------

void listProcesses()
{
	Serial.print(F("Current processes: \n"));
	for (byte pid = 0; pid < MAXPROCESSES; pid++)
	{
		if (p_table[pid].state != '\0')
		{
			printProcess(pid);
		}
	}
}

void printProcess(byte pid)
{
	Serial.print(F("PID: "));
	Serial.print(p_table[pid].pid);
	Serial.print(F(" - State: "));
	Serial.print(p_table[pid].state);
	Serial.print(F(" - Name: "));
	Serial.println(p_table[pid].name);
}

byte start(char *fileName)
{

	if (numOfProcesses >= MAXPROCESSES)
		return -1; // process amount exceeded
	if (checkFATEntry(fileName) == -1)
		return -2; // file does not exist

	int pid;
	for (byte process = 0; process < MAXPROCESSES; process++)
	{
		if (p_table[process].state == '\0')
		{
			pid = process;
			break;
		}
	}
	process proc;
	strcpy(proc.name, fileName);
	proc.pid = pid;
	proc.state = 'r';
	proc.stack.sp = 0;
	proc.fp = getStartPos(checkFATEntry(fileName)); // TODO Functie naar FAT
	proc.pc = 0;
	proc.loop = proc.pc;

	p_table[pid] = proc;
	numOfProcesses++;

	printProcess(pid);

	return 1;
}

//-------------------------------------------------------------------------

void kill(int pid)
{

	strcpy(p_table[pid].name, '\0');
	p_table[pid].state = NULL;
	p_table[pid].pid = NULL;
	p_table[pid].stack = {};
	p_table[pid].fp = NULL;
	p_table[pid].pc = NULL;
	p_table[pid].loop = NULL;
	p_table[pid].sp = NULL;

	//numOfProcesses--;
}

//-------------------------------------------------------------------------

bool checkProcessExist(int pid)
{

	for (byte process = 0; process < MAXPROCESSES; process++)
	{
		if (p_table[process].pid == pid && p_table[process].state != NULL)
			return true;
	}
	return false;
}

//-------------------------------------------------------------------------

bool setProcessState(int pid, char state)
{

	if (checkProcessExist(pid) == -1)
		return false; // check if pid exists
	if (p_table[pid].state == state)
		return false; // check if state is already set

	p_table[pid].state = state;
	return true;
}

//-------------------------------------------------------------------------

bool checkIfStop(int pid)
{
	byte instruction = EEPROM[p_table[pid].fp + p_table[pid].pc++];

	Serial.print(F("PID: "));
	Serial.print(p_table[pid].pid);
	Serial.print(F("- instruction: "));
	Serial.print(EEPROM[p_table[pid].fp + p_table[pid].pc - 1], DEC);
	Serial.print(F("- PC "));
	Serial.println(p_table[pid].pc - 1);

	switch (instruction)
	{
	case (CHAR):
		//p_table[pid].pc++;
		valueToStack(&(p_table[pid]), CHAR);
		break;
	case (INT):
		valueToStack(&(p_table[pid]), INT);
		break;
	case (STRING):
		valueToStack(&(p_table[pid]), STRING);
		break;
	case (FLOAT):
		valueToStack(&(p_table[pid]), FLOAT);
		break;

	case (SET):
		setVar(
				EEPROM[p_table[pid].fp + p_table[pid].pc++],
				p_table[pid].pid,
				&(p_table[pid].stack));
		break;
	case (GET):
		getVar(
				EEPROM[p_table[pid].fp + p_table[pid].pc++],
				p_table[pid].pid,
				&(p_table[pid].stack));
		break;
	case (INCREMENT):
	case (DECREMENT):
	case (UNARYMINUS):
	case (ABS):
	case (SQ):
	case (SQRT):
	case (ANALOGREAD):
	case (DIGITALREAD):
	case (LOGICALNOT):
	case (BITWISENOT):
	case (TOCHAR):
	case (TOINT):
	case (TOFLOAT):
	case (ROUND):
	case (FLOOR):
	case (CEIL):
		unaryInstruction(&p_table[pid], EEPROM[p_table[pid].fp + p_table[pid].pc++]);
		break;
	case (PLUS):
	case (MINUS):
	case (TIMES):
	case (DIVIDEDBY):
	case (MODULUS):
	case (EQUALS):
	case (NOTEQUALS):
	case (LESSTHAN):
	case (LESSTHANOREQUALS):
	case (GREATERTHAN):
	case (GREATERTHANOREQUALS):
	case (MIN):
	case (MAX):
	case (POW):
	case (LOGICALAND):
	case (LOGICALOR):
	case (LOGICALXOR):
	case (BITWISEAND):
	case (BITWISEOR):
	case (BITWISEXOR):
		binaryInstruction(&p_table[pid], EEPROM[p_table[pid].fp + p_table[pid].pc++]);
		break;
	case (CONSTRAIN):
	case (MAP):
	case (PINMODE):
	case (DIGITALWRITE):
	case (ANALOGWRITE):
		arduinoInstruction(&p_table[pid], EEPROM[p_table[pid].fp + p_table[pid].pc++]);
		break;
	case (DELAY):
	case (DELAYUNTIL):
	case (MILLIS):
	case (IF):
	case (ELSE):
	case (ENDIF):
	case (WHILE):
	case (ENDWHILE):
	case (LOOP):
	case (ENDLOOP):
	case (PRINT): // 51
		printInstruction(&p_table[pid]);
		break;
	case (PRINTLN): //52
		printInstruction(&p_table[pid], true);
		break;
	case (STOP):
		setProcessState(p_table[pid].pid, 't');
		return true;
	default:
		Serial.print(F("unknown process instruction: "));
		Serial.println(instruction, DEC);
		setProcessState(p_table[pid].pid, 't');
		return true;
	}
	return false; // Program still running (found no STOP)
}
