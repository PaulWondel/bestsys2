#include "stack.h"

bool hasPeeked = false;

void pushByte(stackType *stack, byte data)
{
    stack->stack[(stack->sp)++] = data;
    //Serial.println(F("Pushed the data to the stack!"));
}

byte popByte(stackType *stack)
{
    return stack->stack[--(stack->sp)];
}

void pushChar(stackType *stack, char c)
{
    pushByte(stack, c);
    pushByte(stack, CHAR);
    //Serial.print(c);
    //Serial.println(F(" pushed CHAR to stack!"));
}

char popChar(stackType *stack)
{
    //popByte(stack);
    char retVal = char(popByte(stack));
    return retVal;
}

void pushInt(stackType *stack, int i)
{
    //assign i on stack with correct endianness TODO:check dit
    pushByte(stack, lowByte(i));
    pushByte(stack, highByte(i));
    pushByte(stack, INT);
    //Serial.print(i);
    //Serial.println(F(" pushed INT to the stack"));
}

int popInt(stackType *stack)
{
    popByte(stack);              // Type
    byte right = popByte(stack); // Right byte
    byte left = popByte(stack);  // Left byte
    return word(left, right);    // Return int
}

void pushFloat(stackType *stack, float f)
{
    byte *b = (byte *)&f; // Convert to byte array

    for (int_least8_t i = 3; i >= 0; i--)
        pushByte(stack, b[i]); // Push byte array

    pushByte(stack, FLOAT); // Type
}

float popFloat(stackType *stack)
{
    popByte(stack); // Type
    float f = 0.0;
    byte *b = (byte *)&f; // Create byte array

    for (int_least8_t x = 0; x < FLOAT; x++)
        b[x] = popByte(stack); // Fill byte array

    return f; // Return float
}

int mystrlen(char *p)
{
    int c = 0;
    while (*p != '\0')
    {
        c++;
        *p++;
    }
    return (c);
}

// WAARSCHUWING: Dit is een compleet ranzig stukje code
void pushString(stackType *stack, char *str)
{
    byte length = mystrlen(str) + 1;

    for (uint_least8_t x = 0; x < length; x++)
        pushByte(stack, str[x]); // Push each char in string

    pushByte(stack, length); // length of the string with terminating zero
    pushByte(stack, STRING); // Type
}

char *popString(stackType *stack, bool popGeneric = true)
{
    popByte(stack); // Type
    uint_least8_t length = popByte(stack); // Length of string
    stack->sp -= length; // Lower stack pointer by length
    return (stack->stack + stack->sp); // Return start address of string
}

float peek(stackType *stack)
{
    hasPeeked = true;
    return popVal(stack);
}

float popVal(stackType *stack)
{
    //pop one value from the stack to check what it is
    //then push it back in order to retain it if nessescary
    float f;
    byte genericData = popByte(stack);
    // pushByte(stack, genericData);

    switch (genericData)
    {
    case CHAR:
        f = (float)popChar(stack);

        if (hasPeeked)
        {
            pushChar(stack, (char)f);
            hasPeeked = false;
        }
        break;

    case INT:
        f = (float)popInt(stack);

        if (hasPeeked)
        {
            pushInt(stack, (int)f);
            hasPeeked = false;
        }
        break;

    case FLOAT:
        f = popFloat(stack);
        if (hasPeeked)
        {
            //pushFloat(stack, f);
        }
        break;

    case STRING:
        f = NULL;

        if (hasPeeked)
        {
            pushByte(stack, STRING);
        }
    }

    return f;
}
