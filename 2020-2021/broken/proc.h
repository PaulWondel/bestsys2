#ifndef PROC_H
#define PROC_H    

#define MAXFILENAMESIZE 12
#define MAXPROCESSES 10
#define RUNPROCESSTIME 50 // Time in millis for each program cycle

#include<Arduino.h>
#include "stack.h"
#include "ardutry3.h"
#include "instruction_set.h"

typedef struct
{
    char name[MAXFILENAMESIZE];
    char state; // running(r) paused(p) terminated(t)
    byte pid; // process id
    int pc; // program pointer
    int fp; // file pointer
    int sp; // stack pointer
    int loop;
    stackType stack;
}process;

// Variables
byte start(char* fileName);

// Functions prototypes
void printProcess(byte pid);
void listProcesses();
void runPrograms();
byte getNumOfRunningProgramms();

//int suspend(int pid);
void kill(int pid);
//void listProcesses();
bool checkProcessExists(int pid);
bool setProcessState(int index, char state);
bool checkIfStop(int pid);

#endif
