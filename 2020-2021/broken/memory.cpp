#include "memory.h"

byte varId = 0;
byte mem[MEMSIZE];
varStruct v[MEMTABLE];
varStruct emptyVar;

bool setVar(char varName, int pid, stackType* stack)
{
    if(varId > MEMTABLE)return false; //no free memory left

    //int index = varIndex(varName, pid);
    //if(index >= 0 && varIndex < varId)
    //{
     //   //remove var to create space if needed
    //    eraseVar(index);
    //}

    //write var
    v[varId].varName = varName;
    v[varId].pid = pid;
    v[varId].varType = popByte(stack);

    //set size of type to be size of var
    if(v[varId].varType == STRING)
    {
        v[varId].size = popByte(stack);
    }
	else // CHAR INT FLOAT
	{ 
		v[varId].size = v[varId].varType;
	}

    v[varId].addr = varStartPos(v[varId].size);
	
	//check if enough size available for data
    if(v[varId].addr == -1)return false;

    for(int i = v[varId].size -1; i >= 0; i--)
    {
        mem[v[varId].addr +i] = popByte(stack);
    }


    varId++;
    return true;
}

stackType* getVar(char varName, int pid, stackType* stack)
{
    int index = varIndex(varName, pid);
	
    //push data,length and type onto stack
    for(int i = 0; i < v[index].size; i++)
    {
        pushByte(stack, mem[v[index].addr + i]);
    }

    if(v[index].varType == STRING)
    {
        pushByte(stack, v[index].size);
    }

    pushByte(stack, v[index].varType);

    return stack;
}

void clearMem(int pid)
{
    for(byte i = 0; i < varId; i++)
    {
        if(v[i].pid == pid)
        {
            eraseVar(i--);
        }
    }
}

void eraseVar(byte i)
{
    while(v[i + 1].varName != NULL)
    {
        v[i] = v[i +1];
        i++;
    }
    v[i] = emptyVar;
}

int varStartPos(int size)
{
    if(varId == 0 && size <= MEMSIZE)
    {
        return 0; // found first address
    }

    for(int i = 0; i < varId - 1; i++)
    {
        if(v[i+1].addr = (v[i].addr + v[i].size) >= size)
        {
            return v[i].addr + v[i].size;
        }
    }

    if(MEMSIZE - (v[varId-1].addr + v[varId-1].size) >= size)
    {
        return v[varId-1].addr + v[varId-1].size;
    }

    return -1;
}

int varIndex(char varName, int pid)
{
    for(int i = 0; i < MEMTABLE; i++)
    {
		/*Serial.print(v[i].varName);
		Serial.print(F(" "));
		Serial.print(v[i].pid);
		Serial.print(F(" "));
		Serial.print(varName);
		Serial.print(F(" "));
		Serial.print(pid);
		Serial.println(F(" "));*/
		
		
        if(v[i].varName == varName && v[i].pid == pid)
        {
            return i;
        }
    }
    return -1;
}