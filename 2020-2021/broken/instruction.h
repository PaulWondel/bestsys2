#ifndef INSTRUCTION_HPP
#define INSTRUCTION_HPP

#include <Arduino.h>
#include "proc.h"
#include "stack.h"

void delayFuncs(process *proc, int type);
void valueToStack(process  *proc, int type);
void arduinoInstruction(process *proc, int type);
void unaryInstruction(process *proc, int type);
void binaryInstruction(process *proc, int type);
void printInstruction(process *proc, bool newline=false);


#endif
