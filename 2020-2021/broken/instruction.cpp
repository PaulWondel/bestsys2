#include "instruction.h"
#include "stack.h"
#include "instruction_set.h"

void delayFuncs(process *proc, int type){

	if(type == DELAY){
	delay(2);
	}
	
}

void valueToStack(process *proc, int type)
{	
	switch(type){
		case(CHAR):
			pushByte(&(proc->stack), EEPROM[proc->pc++ + proc->fp]);
			pushByte(&(proc->stack), type);
			break;
		case(STRING):
			//Serial.println(F("STR"));		
			int size = 0;
			char nextChar = 0;
			char *str = malloc(50);
			
			do
			{
				nextChar = EEPROM[proc->pc++ + proc->fp];
				size++;
				*str++ = nextChar;
			}
			while (nextChar != 0);
			
			pushString(&(proc->stack), str-size);
			free(str);
			break;
		default:
			Serial.println(F("DEFAULT"));
			for(byte i = 0; i < type; i++){
				pushByte(&(proc->stack), EEPROM[proc->pc++ + proc->fp]);
			}
			pushByte(&(proc->stack), type);
		break;
	}
}


void arduinoInstruction(process *proc, int type)
{
	switch (type)
	{	
		case (CONSTRAIN):
		//TODO
		case (MAP):
		//TODO
		case (PINMODE):
		case (DIGITALWRITE):
		case (ANALOGWRITE):	
			{
				//pop value and pin nr
				int val = (int)popVal(&(proc->stack));
				int pin = (int)popVal(&(proc->stack));

				if (type == PINMODE)
				{
					pinMode(pin, val);
				}
				else if (type == ANALOGWRITE)
				{
					analogWrite(pin, val);
				}
				else if (type == DIGITALWRITE)
				{
					digitalWrite(pin, val);
				}	
			}
		break;
	
	default:
		Serial.println(F("Unrecognized Arduino instruction! (code): "));
		Serial.print(type);
		break;
	}
}



void unaryInstruction(process *proc, int type)
{

	//push a byte, peek what value it is and if we can use it, then pop it back onto the stack
	byte genericVal = popByte(&(proc->stack));
	pushByte(&(proc->stack), genericVal);

	//then pop the data with popVal to get the entire floating point range from the stack
	float val = popVal(&(proc->stack));

	switch (type)
	{
		case (INCREMENT):
            val = val + 1.0f;
            break;
        case (DECREMENT):
            val = val - 1.0f;
            break;
        case (UNARYMINUS):
            val = val * -1.0f;
            break;
        case (ABS):
            val = (val >= 0 ? val : val*-1.0f);
            break;
        case (SQ):
            val = val * val;
            break;
        case (SQRT):
            val = sqrt(val);
            break;
        case (ANALOGREAD):
            val = analogRead(val);
            type = INT;
            break;
        case (DIGITALREAD):
            val = digitalRead(val);
            type = CHAR;
            break;
        case (LOGICALNOT):
            val = (val == 0.0f ? 1 : 0);
            type = CHAR;
            break;
        case (BITWISENOT):
            val = ~(int) val; //use ~ operator to invert the val bitwise
            break;
        case (TOCHAR):
            val = round(val);
            type = CHAR;
            break;
        case (TOINT):
        case (ROUND):
            val = round(val) + 0;
            type = INT;
            break;
        case (TOFLOAT):
            type = FLOAT;
            break;
        case (FLOOR):
            val = floor(val);
            type = INT;
            break;
        case (CEIL):
            val = ceil(val);
            type = INT;
        default:
            Serial.print(F("Unrecognized unary instruction! (code): "));
            Serial.println(type);
            break;
	}
}

void binaryInstruction(process *proc, int type)
{
	//first, peek both datatypes to determine if we are working with the right data 
	//after that, we use the popVal() to get the value into the largest datatype available
	byte peekY = popByte(&(proc->stack));
	pushByte(&(proc->stack), peekY);
	float y = popVal(&(proc->stack));

	byte peekX =  popByte(&(proc->stack));
	pushByte(&(proc->stack), peekX); 
	float x =  popVal(&(proc->stack));

	float ans;

	switch (type)
	{
		case (PLUS):
			ans = x + y;
			break;
		case (MINUS):
			ans = x - y;
			break;
		case (TIMES):
			ans = x * y;
			break;
		case (DIVIDEDBY):
			ans = x / y;
			break;
		case (MODULUS):
			ans = (int) x % (int) y;
			break;
		case(EQUALS):
			ans = (x == y ? 1 : 0);
			break;
		case(NOTEQUALS):
			ans = (x != y ? 1: 0);
			break;
		case(LESSTHAN):
			ans = (x < y ? 1 : 0);
			break;
		case(LESSTHANOREQUALS):
			ans = (x <= y ? 1 : 0);
			break;
		case(GREATERTHAN):
			ans = (x > y ? 1 : 0);
			break;
		case(GREATERTHANOREQUALS):
			ans = (x >= y ? 1 : 0);
			break;
		case (MIN):
			ans = (x < y ? x : y);
			break;
		case (MAX):
			ans = (x > y ? x : y);
			break;
		case (POW):
			for (int i = 0; i < y; i++)
				ans *= x;
			break;
		case (LOGICALAND):
			ans = (x && y ? 1 : 0);
			break;
		case(LOGICALOR):
			ans = (x || y ? 1 : 0);
			break;
		case(LOGICALXOR):
			ans = (!x != !y ? 1 : 0);
			break;
		case (BITWISEAND):
			ans = (int) x & (int) y;
			break;
		case (BITWISEOR):
			ans = (int) x | (int) y;
			break;
		case (BITWISEXOR):
			ans = (int) x ^ (int) y;
			break;
		default:
            Serial.print(F("Unrecognized unary instruction! (code): "));
            Serial.println(type);
	}
}




void printInstruction(process *proc, bool newline=false){

	byte type = popByte(&(proc->stack));

	switch(type){
	case(CHAR):
		Serial.print(popChar(&(proc->stack)));
		break;
	case(STRING):
		Serial.print(popString(&(proc->stack), false));
		break;
	case(INT):
		Serial.print(popInt(&(proc->stack)));
		break;
	case(FLOAT):
		Serial.print(popFloat(&(proc->stack)), 5);
		break;
	}
	if(newline) Serial.println();
}
