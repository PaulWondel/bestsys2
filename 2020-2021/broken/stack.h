#ifndef STACK_H
#define STACK_H

#include "instruction_set.h"
#include <Arduino.h>

#define STACKSIZE 32

typedef struct 
{
    byte stack[STACKSIZE];
    uint_least8_t sp = 0; 
}stackType;


void pushByte(stackType* stack, byte data);
byte popByte(stackType* stack);

void pushChar(stackType* stack, char c);
char popChar(stackType* stack);

void pushInt(stackType* stack, int i);
int popInt(stackType* stack);

void pushFloat(stackType* stack, float f);
float popFloat(stackType* stack);

void pushString(stackType* stack, char *str);
char* popString(stackType* stack, bool popGeneric=true);

float popVal(stackType* stack);
float peek(stackType* stack);

#endif
