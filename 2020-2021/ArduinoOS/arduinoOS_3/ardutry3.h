#ifndef ARDUTRY3_H
#define ARDUTRY3_H

#include <EEPROM.h>

const int BUFFSIZE = 12; // Size of the buffer
const int BUFF_ARG_SIZE = 60;

const int nameSize = 12;
const int parameterSize= 10;
const int descriptSize= 30;

const int FILENAME_SIZE = 12; // Size file name
const int FAT_START = 1;
const int FAT_SIZE_MAX = 10; // 10 FAT positions only, each one 16 bytes

const int STACKSIZE = 32;
const int TABLE_SIZE = 25;
const int MEMORY_SIZE = 256;

//==============================================================================
//== Structs

// Defines a function using a struct
typedef struct {
  char name[BUFFSIZE];
  void (*func)();
}commandType;

typedef struct{
  char name[nameSize];
  char parameter[parameterSize];
  char descr[descriptSize];
}commandList;

// EEPROM struct
typedef struct{
  char filename[FILENAME_SIZE];
  int filePosition;
  int fileSize;
}eepromfile;

// Memory table struct
typedef struct{
  char name;
  byte type;
  unsigned int size;
  unsigned int address;
  int processID;
}memoryTable;

// Stack struct
typedef struct{
  byte stack[STACKSIZE];
  byte sp = 0;
}stackType;

//==============================================================================
//== Beschikbare commando's in de commandline interface
void store();       // sla een bestand op in het bestandssysteem
void retrieve();    // vraag een bestand op uit het bestandssysteem
void erase();       // wis een bestand
void files();       // print een lijst van bestanden
void freespace();   // print de beschikbare ruimte in het bestandssysteem
void run();         // start een programma
void list();        // print een lijst van processen
void suspend();     // pauzeer een proces
void resume();      // hervat een proces
void kill();        // kill stop een proces
void help();        // print lijst met bestaande commandos

void callFunction();
void buffer();
void printCommandList();
void checkForCommand();
bool writeCommand(char bufferReceived);
int writeArgument(int argumentNumber, char bufferReceived);
void emptyBuffer();

// EEPROM Functions
bool writeFATEntry(byte position, eepromfile file);
eepromfile readFATEntry(byte position);
int storeFile(char* name, int size, byte* data);
int checkFATEntry(char* name);
void writeData(int startPosition, int size, byte* data);
int findFreeSpace(int size);
int getStartPos(int currentIndex);
int getNextFileStartPos(int index);
int getFileSize(int index);
int indexZeroFile();

// MEMORY Functions
bool setVar(byte name, int processID);
byte getVar();
void clearVar();
void clearAllVars(int processID);
int existsInMemory(byte name, int processID);
bool deleteEntry(byte index);

// STACK Functions
void pushByte(byte b);
byte popByte();

// EXTRA Functions
void formatFAT();
void writeTestPrograms();

//==============================================================================
//== Lists

// Places a command in a array
static commandType command[] = {
  {"store",         &store},
  {"retrieve",      &retrieve},
  {"erase",         &erase},
  {"files",         &files},
  {"freespace",     &freespace},
  {"run",           &run},
  {"list",          &list},
  {"suspend",       &suspend},
  {"resume",        &resume},
  {"kill",          &kill},
  {"help",          &help},
  {"format",        &formatFAT}
};

static commandList infoList[] = {
  {"store", "file,size,data", "Store files in filesystem"},
  {"retrieve", "file", "Retrieve file from filesystem"},
  {"erase", "file", "Erase file from filesystem"},
  {"files", "", "Lists files present in filesystem"},
  {"freespace", "", "Free space in filesystem"},
  {"run", "", "Run program"},
  {"list", "", "List running processes"},
  {"suspend", "id", "Suspend process"},
  {"resume", "id", "Resume process"},
  {"kill", "id", "Kill process"},
  {"help", "", "list the available commands"},
  {"format", "", "format files in EEPROM"}
};

//==============================================================================



#endif