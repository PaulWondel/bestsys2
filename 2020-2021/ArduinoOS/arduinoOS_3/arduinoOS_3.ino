/*
ArduinOS 3.0
Besturingssystemen 2
TINBES02-2
Hogeschool Rotterdam
 
Gemaakt door:
- Rowalski Wever     (0918221@hr.nl)
- Paul Wondel        (0947421@hr.nl)
*/

#include "ardutry3.h"
#include "test_programs.h"

// Buffer variables
static int commandSize = sizeof(command) / sizeof(commandType);
static int commandListSize = sizeof(infoList) / sizeof(commandList); 
bool incomingData = false;        // check voor serial input
bool firstCommand = false;         // eerste commando
int currentArgNumber = 0;         // argument teller
bool existingCommand = false;     // check voor bestaand commando

char commandBuffer[BUFFSIZE];
char arg1[BUFFSIZE];
char arg2[BUFFSIZE];
char arg3[BUFFSIZE];

// FAT variables
EERef noOfFiles = EEPROM[0];

// MEMORY variables
byte memory[MEMORY_SIZE];             // can be seen as RAM
memoryTable memTable[TABLE_SIZE];
int noOfVars = 0;

void setup() {
  Serial.begin(9600);  
  // storeFile("p3", sizeof(prog3), prog3);
  Serial.print(F("<Welcome to Arduino OS 3!>\n"));
  Serial.print(F("<Using buffer 2.0!>\n"));
}

void loop() {
  buffer();
}

//==============================================================================
//== Command line

// Serial buffer from the input
void buffer() {
  char answer;
 
  while (Serial.available() > 0 && incomingData == false)
  {
    answer = Serial.read();
    if (answer != '\n') 
    {
      if (firstCommand != true){
        firstCommand = writeCommand(answer);        
      }
      else {
        // writeArgument(currentArgNumber,answer);
        currentArgNumber = writeArgument(currentArgNumber,answer);
        //checkForCommand();
      }
      return;
    }    
    
    incomingData = true;
    checkForCommand();
    printInputData();
    emptyBuffer();
    return;
  }
}

void checkForCommand()
{
  // Serial.println("Checking for command");
  if(incomingData){
    for (int i = 0; i < commandSize; i++)
    {
      if (strcmp(commandBuffer, command[i].name) == 0)
      {
        void (*func)() = command[i].func;
        func();
        existingCommand = true;
        // break;
        return;
      }
    }
    if(existingCommand)
    {
      incomingData = false;
      existingCommand = false;
      return;
    }
    else{
    Serial.print(F("Command unknown."));
    Serial.println(F(" Use commands from the following list: "));
    printCommandList();
    }       
    // Serial.print(F((String)"Command "+commandBuffer+"' unknown."));
    
    incomingData = false; 
  }
}

//==============================================================================
//== Writing helper functions

bool writeCommand(char bufferReceived){
  if (bufferReceived == ' ' || bufferReceived == '\n'){
    return true;
  }

  int bufferSize = strlen(commandBuffer);

  if (bufferSize >= BUFFSIZE)
  {
    //this is suppose to check for overflow, or commands longer than 11 chars
    commandBuffer[BUFFSIZE - 1] = bufferReceived;
    return false;
  }
  commandBuffer[bufferSize] = bufferReceived;
  return false;
}

int writeArgument(int argumentNumber, char bufferReceived){
  if (bufferReceived == ' ' || bufferReceived == '\n'){
    argumentNumber += 1; 
    return argumentNumber;
  }
  if (currentArgNumber == 0){
    int bufferSize = strlen(arg1);

    if (bufferSize >= BUFFSIZE)
    {
      arg1[BUFFSIZE - 1] = bufferReceived;
      return argumentNumber;
    }
    arg1[bufferSize] = bufferReceived;
    return argumentNumber;
  }
  else if (currentArgNumber == 1){
    int bufferSize = strlen(arg2);

    if (bufferSize >= BUFFSIZE)
    {
      arg2[BUFFSIZE - 1] = bufferReceived;
      return argumentNumber;
    }
    arg2[bufferSize] = bufferReceived;
    return argumentNumber;
  }
  else if (currentArgNumber == 2){
    int bufferSize = strlen(arg3);

    if (bufferSize >= BUFFSIZE)
    {
      arg3[BUFFSIZE - 1] = bufferReceived;
      return argumentNumber;
    }
    arg3[bufferSize] = bufferReceived;
    return argumentNumber;
  }
  return argumentNumber;
}

//==============================================================================
//== Empty the buffer
void emptyBuffer(){
  // Serial.println("PURGE BUFFER");
  for (int i = 0; i < sizeof(commandBuffer); i++)
  {
    commandBuffer[i]='\0';
  }  
  for (int i = 0; i < sizeof(arg1); i++)
  {
    arg1[i]='\0';
  }
  for (int i = 0; i < sizeof(arg2); i++)
  {
    arg2[i]='\0';
  }
  for (int i = 0; i < sizeof(arg3); i++)
  {
    arg3[i]='\0';
  }
  currentArgNumber = 0;
  firstCommand = false;
}

//==============================================================================
//== Print functions

// Print Contents of the Buffer
void printInputData() {
    if (incomingData) 
    {
      Serial.print(F("Previous input: "));
      Serial.print(commandBuffer);
      Serial.print(F(" "));
      Serial.print(arg1);
      Serial.print(F(" "));
      Serial.print(arg2);
      Serial.print(F(" "));
      Serial.println(arg3);
      
      incomingData = false;
    }
}

// Print the list of commands with description
void printCommandList(){
  char commandName;
  for (int i = 0; i < commandListSize; i++)
  {
    Serial.print(infoList[i].name);
    Serial.print(" - ");
    Serial.println(infoList[i].descr);
  }
  Serial.print("\n");
}

//==============================================================================
//== Commando's Functions
void store(){
  // Serial.println("This is the store function");
  int filesize = atoi(arg2)+1;
  int errorCode = storeFile(arg1, filesize, (byte*) arg3);
  switch (errorCode) // Error code
  {
    case (1):
        Serial.print(F("stored: "));
        Serial.println(arg1);
        break;
    case (-1):
        Serial.println(F("filename too long"));
        break;
    case (-2):
        Serial.println(F("insufficient space"));
        break;
    case (-3):
        Serial.println(F("filename already exists"));
  }
  
}

void retrieve(){
  // Serial.println(F("This is the retrieve function"));

  int index = checkFATEntry(arg1);
  
  if (index == -1){
    Serial.println(F("File not found!")); // File does not exist
    return;
  }
      
  int startPos = getStartPos(index); // Get start address
  int size = getFileSize(index); // Get file size

  char byteFATData[size]; // byte array for bytes

  for (int i = 0; i < size; i++)
  {
    byteFATData[i] = EEPROM[startPos+i], DEC;
  }
  Serial.println(byteFATData);
}

void erase(){
  // Serial.println(F("This is the erase function"));

  int location = checkFATEntry(arg1);
  if (location != -1){
    eepromfile file = readFATEntry(location);
    file.filename[0] = '\0'; // Clear filename
    file.filePosition = 0;
    file.fileSize = 0;
    writeFATEntry(location, file); // Write empty FAT entry
    noOfFiles -= 1; // file counter -1
  }
}

void files(){
  // Serial.println(F("This is the files function"));
  eepromfile file;
  for (byte i = 0; i < FAT_SIZE_MAX; i++) // Go through all files in FAT
  {
    file = readFATEntry(i);
    if (file.fileSize > 0) // Only print files that contain data
    {
      Serial.print(F("file: "));
      Serial.print(i); // File index from FAT
      Serial.print(F("\t- "));
      Serial.print(file.filename); // Filename
      Serial.print(F("\t\t("));
      Serial.print(file.filePosition); // byte on disk
      Serial.print(F("/"));
      Serial.print(file.filePosition + file.fileSize - 1); // End byte on disk
      Serial.print(F(" - "));
      Serial.print(file.fileSize); // Filesize
      Serial.println(F(" bytes)"));
    }
    else{
      Serial.println(F("No file saved"));
    }
  }
}

void freespace(){
  int freeSpace = EEPROM.length(); // Get EEPROM size

  freeSpace -= sizeof(noOfFiles); // Substract noOfFiles
  freeSpace -= (sizeof(eepromfile) * FAT_SIZE_MAX); // Substract FAT size

  for (byte i = 0; i < FAT_SIZE_MAX; i++) // Go trough all files in FAT
  {
    eepromfile file = readFATEntry(i);
    if (file.fileSize != 0) freeSpace -= file.fileSize;
  }
  Serial.print(F("freeSpace: "));
  Serial.print(freeSpace);
  Serial.print(F(" bytes\n"));
}

void run(){
  Serial.println(F("Not implemented yet"));
}

void list(){
  Serial.println(F("Not implemented yet"));
}

void suspend(){
  Serial.println(F("Not implemented yet"));
}

void resume(){
  Serial.println(F("Not implemented yet"));
}

void kill(){
  Serial.println(F("Not implemented yet"));
}

void help(){
  Serial.println(F("The following commands are available:"));
  for (int i = 0; i < commandSize; i++) {
    Serial.print("- ");
    Serial.println(command[i].name);
  }
}

//==============================================================================
//== FAT Function

// write file to FAT
bool writeFATEntry(byte position, eepromfile file)
{
  EEPROM.put(FAT_START + position * sizeof(eepromfile), file);
  return;
}

eepromfile readFATEntry(byte position)
{
  eepromfile file;
  EEPROM.get(FAT_START + position * sizeof(eepromfile), file);
  return file;
}

// create file and alloc
int storeFile(char* name, int size, byte* data){
  if(sizeof(name) > FILENAME_SIZE){
    return -1;
  }
  if (noOfFiles == FAT_SIZE_MAX) // Check FAT size
        return -2;
  if (checkFATEntry(name) != -1) // Check if name does not exist
      return -3;

  int position = findFreeSpace(size);
  if(position == -1)
  {
    return -2; //no space available
  }
  
  eepromfile file = (eepromfile){"", position, size};
  strcpy(file.filename, name);
  writeFATEntry(indexZeroFile(), file); // Store file in FAT
  writeData(file.filePosition, size, data);

  noOfFiles += 1; // 1 eternity later, a file added
  return 1;
}

int checkFATEntry(char* name)
{
  for (byte i = 0; i < FAT_SIZE_MAX; i++) // Go through all files in FAT
  {
      eepromfile file = readFATEntry(i);
      if (strcmp(file.filename, name) == 0) return i; // Return entry ID
  }
  return -1; // Name does not exist
}

void writeData(int startPosition, int size, byte* data)
{
  int nextAddress = 0;
  for (int i = 0; i < size; i++) // Store all bytes to disk
  {
    EEPROM.write(startPosition + nextAddress, data[i]); // Store byte on EERPOM
    nextAddress += sizeof(data[i]); // Skip to next address
  }
}

int findFreeSpace(int size){
  int firstFreeByte = sizeof(noOfFiles) + (sizeof(eepromfile) * FAT_SIZE_MAX) + 1;

  if (noOfFiles == 0)
      return firstFreeByte;  // Write first file byte after FAT

  // Check space between files or space untill end of EEPROM
  bool firstFile = true;
  for (byte i = 0; i < FAT_SIZE_MAX; i++) // Already checked 0, so start at 1
  {
    eepromfile file = readFATEntry(i); // Get file
    if (file.fileSize > 0) // Check if file has data
    {
      // Check the first file in FAT
      if (firstFile && (file.filePosition - firstFreeByte > size))
        return firstFreeByte; // Write data after FAT
      firstFile = false;

      // Check if there is space between current file and next
      //  file (or EERPOM end if no more files exist)
      if (size < getNextFilePosition(i) - (file.filePosition + file.fileSize))
        return (file.filePosition + file.fileSize); // Write after file
      }
  }
  return -1; // space not enough
}

// Get startPos from file index
int getStartPos(int currentIndex)
{
    eepromfile file = readFATEntry(currentIndex);
    return file.filePosition; // Return the start postion on EEPROM
}

int getNextFilePosition(int index)
{
    for (byte n = index + 1; n < FAT_SIZE_MAX; n++) // Go through all files in FAT
    {
        eepromfile file = readFATEntry(n);
        if (file.fileSize != 0) return file.filePosition;
    }
    return (EEPROM.length() + 1); // Add one so we can use last byte of EEPROM
}

// Get file size from file index
int getFileSize(int index)
{
    eepromfile file = readFATEntry(index);
    return file.fileSize; // Return file size
}

int indexZeroFile()
{
  for (byte i = 0; i < FAT_SIZE_MAX; i++) // Go through all files in FAT
  {
      eepromfile file = readFATEntry(i);
      if (file.fileSize == 0) return i; // Return first free file
  }
  return -1;
}

void formatFAT()
{
  noOfFiles = 0; 
  for (byte i = 0; i < FAT_SIZE_MAX; i++)
  {
    eepromfile emptyFile = (eepromfile){"",0,0};
    writeFATEntry(i, emptyFile);
  }
}

//==============================================================================
//== Memory functions

bool setVar(byte name, int processID, stackType* stack){

  if(noOfVars >= TABLE_SIZE)
  {
    return false; //memory is full
  }

  int inMemory = existsInMemory(name, processID);
  if (inMemory != -1)
  {
    deleteEntry(inMemory);
  }

  byte type = popByte(stack);
  int size = getSize(type, stack);

  int address = getStartPos(size);

  memoryTable entry;
  entry.name = name;
  entry.processID = processID;
  entry.type = type;
  entry.address = address;
  entry.size = size;

  memTable[noOfVars] = entry;

  for (int i = (memTable[noOfVars].size - 1); i >= 0; i--)
  {
    byte data = popByte(stack);
    memory[memTable[noOfVars].address + i] = data;
    // Serial.println(data);
  }

  ++noOfVars;
  return true;

//take value from stack (string or int) and save it in memory
//check beforehand if there is enough room in memory, if not print error
//check after for duplicate (name +process id)
//if it exists remove it from the memory table (overwrite)
//when overwriting all other entries need to move up, and -1 from noofvars
}

byte getVar(){
  
}

int existsInMemory(byte name, int processID)
{
  if (noOfVars <= 0)
  {
    return -1;
  }

  for(byte i = 0; i < noOfVars; i++)
  {
    if (name ==memTable[i].name && processID == memTable[i].processID)
    {
      return i;
    }
  }
  return -1;
}

bool deleteEntry(byte index)
{
  while (memTable[index + 1].name != NULL)
  {
    memTable[index] = memTable[index + 1];
    ++index;
  }

  memTable[index] = memoryTable{};

  noOfVars--;
  return true;
}

void clearVar(){

}

void clearAllVars(int processID){
  
}

//==============================================================================
//== Stack functions

void pushByte (stackType* stack, byte b){
  stack->stack[(stack->sp)++] = b;
}

byte popByte(stackType* stack) {
  return stack->stack[--(stack->sp)];
}

//==============================================================================
//== EXTRA functions

void writeTestPrograms(){
  // storeFile("getset", sizeof(getset), getset);
  // storeFile("calc2", sizeof(calc), calc);
  // storeFile("p1", sizeof(prog1), prog1);
  // storeFile("p2", sizeof(prog2), prog2);
  storeFile("p3", sizeof(prog3), prog3);
  //storeFile("p4", sizeof(prog4), prog4);
  //storeFile("p5", sizeof(prog5), prog5);
  //storeFile("p6", sizeof(prog5), prog6);
}