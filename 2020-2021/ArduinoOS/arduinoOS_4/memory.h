#ifndef MEMORY_H
#define MEMORY_H

//#include <Arduino.h>
#include "stack.h"
#include "instruction_set.h"

#define MEMSIZE 10
#define MEMTABLE 25

typedef struct
{
    char varName;
    unsigned int pid;
    int varType;
    byte size;
    unsigned int addr;
} varStruct;

int varIndex(char varName, int pid);
bool setVar(char varName, int pid, stackType* stack);
int getVar(char varName, int pid, stackType* stack);
void clearMem(int pid);
void eraseVar(byte index);
int varStartPos(int size);

#endif

