#include "process.h"
#include "instruction.h"
#include "memory.h"

#define RUNNING 'r'
#define SUSPEND 's'
#define TERMINTED 't'

process p_table[MAXPROCESSES];
byte numOfProcesses = 0;

//-------------------------------------------------------------------------

int processStart(char *fileName)
{
	// Check amount of processes
	if (numOfProcesses >= MAXPROCESSES)
		return -1; // To many processes

	// Check if file exists
	int index = checkFATEntry(fileName);
	if (index == -1)
		return -2; // File not found

	// get first free process ID
	int id;
	for (byte i = 0; i < MAXPROCESSES; i++)
	{
		if (p_table[i].state == '\0')
		{
			id = i;
			break;
		}
	}

	// Create process in process table
	process proc;
	strcpy(proc.name, fileName);
	proc.pid = id;
	proc.state = 'r';
	proc.stack.sp = 0;
	proc.fp = 0;
	proc.pc = getStartPos(index);
	proc.loop = proc.pc;

	// Store process in process table
	p_table[id] = proc;
	numOfProcesses++;
	return 1;
}

//==============================================================================
// Go through the running processes and execute them one by one
void runPrograms()
{
	// Check state of all processID's
	int nrOfRunningProcesses = 0; // Check amount of runnnig programs
	for (byte i = 0; i < MAXPROCESSES; i++)
	{
		if (p_table[i].state == 'r')
		{
			nrOfRunningProcesses++;
			// Serial.print(F("Enter running loop\n"));
		}
		if (p_table[i].state == 't')
		{
			// Serial.print(F("Killing process\n"));
			kill(i);
		} // Kill process
	}
	if (nrOfRunningProcesses == 0)
		return; // Nothing is running

	// Setup program time handling
	unsigned long startTime = 0;
	unsigned long runTime = ceil((double)RUNPROCESSTIME /
															 (double)nrOfRunningProcesses);

	// Execute running programs while runTime is not exceeded
	// Credit: Thomas Graafland
	for (byte i = 0; i < MAXPROCESSES; i++)
	{
		startTime = millis(); // startTime when program started running
		while (p_table[i].state == 'r' && (millis() - startTime < runTime))
		{
			// Execute a line of the program
			bool terminated = execute(i);
			if (terminated)
			{
				i--;
				break; // Goto next program in for loop
			}
		}
	}
}

//-------------------------------------------------------------------------

void listProcesses()
{
	//ok, run it? | yeh

	Serial.print(F("Current processes: \n"));
	for (byte pid = 0; pid < MAXPROCESSES; pid++)
	{
		if (p_table[pid].state != '\0')
		{
			printProcess(pid);
		}
	}
}

void printProcess(byte pid)
{
	Serial.print(F("PID: "));
	Serial.print(p_table[pid].pid);
	Serial.print(F(" - State: "));
	Serial.print(p_table[pid].state);
	Serial.print(F(" - Name: "));
	Serial.println(p_table[pid].name);
}


//-------------------------------------------------------------------------

void kill(int pid)
{
	clearMem(pid);

	process process = p_table[pid];
	Serial.print(F("Killed process: "));
	Serial.print(process.name);
	strcpy(process.name, '\0');
	process.state = '\0';
	process.pid = NULL;
	process.pc = NULL;
	process.fp = NULL;
	process.sp = NULL;
	process.loop = NULL;
	process.stack = {};
	p_table[pid] = process;

	numOfProcesses--;
}

//-------------------------------------------------------------------------

bool checkProcessExist(int pid)
{

	for (byte process = 0; process < MAXPROCESSES; process++)
	{
		if (p_table[process].pid == pid && p_table[process].state != NULL)
			return true;
	}
	return false;
}

//-------------------------------------------------------------------------

bool setProcessState(int pid, char state)
{

	if (checkProcessExist(pid) == -1)
		return false; // check if pid exists

	if (p_table[pid].state == state)
		return false; // check if state is already set

	p_table[pid].state = state;
	return true;
}

//-------------------------------------------------------------------------

bool execute(int index)
{
	byte nextInstruction = EEPROM[p_table[index].pc++];


	switch (nextInstruction)
	{
	// Datatpyes
	case (CHAR):
	case (INT):
	case (STRING):
	case (FLOAT):
		valueToStack(p_table + index, nextInstruction);
		break;

	// Variables
	case (SET):
		setVar(EEPROM[p_table[index].pc++], // Name
									 p_table[index].pid,				 // ProcessID
									 &(p_table[index].stack));				 // Stack pointer
		break;
	case (GET):
		getVar(EEPROM[p_table[index].pc++], // Name
									 p_table[index].pid,				 // ProcessID
									 &(p_table[index].stack));				 // Stack pointer
		break;

	// Unary operations
	case (INCREMENT):
	case (DECREMENT):
	case (UNARYMINUS):
	case (ABS):
	case (SQ):
	case (SQRT):
	case (ANALOGREAD):
	case (DIGITALREAD):
	case (LOGICALNOT):
	case (BITWISENOT):
	case (TOCHAR):
	case (TOINT):
	case (TOFLOAT):
	case (ROUND):
	case (FLOOR):
	case (CEIL):
		unaryOperation(p_table + index, nextInstruction);
		break;

	// Binary operations
	case (PLUS):
	case (MINUS):
	case (TIMES):
	case (DIVIDEDBY):
	case (MODULUS):
	case (EQUALS):
	case (NOTEQUALS):
	case (LESSTHAN):
	case (LESSTHANOREQUALS):
	case (GREATERTHAN):
	case (GREATERTHANOREQUALS):
	case (MIN):
	case (MAX):
	case (POW):
	case (LOGICALAND):
	case (LOGICALOR):
	case (LOGICALXOR):
	case (BITWISEAND):
	case (BITWISEOR):
	case (BITWISEXOR):
		binaryOperation(p_table + index, nextInstruction);
		break;

	// Arduino operations
	case (CONSTRAIN):
	case (MAP):
	case (PINMODE):
	case (DIGITALWRITE):
	case (ANALOGWRITE):
		arduinoOperation(p_table + index, nextInstruction);
		break;

	// Time operations
	case (DELAY):
	case (DELAYUNTIL):
	case (MILLIS):
		timeOperation(p_table + index, nextInstruction);
		break;

	// Time operations
	case (IF):
	case (ELSE):
	case (ENDIF):
	case (WHILE):
	case (ENDWHILE):
	case (LOOP):
	case (ENDLOOP):
		branchOperation(p_table + index, nextInstruction);
		break;

	// Print
	case (PRINT):
		print(p_table + index);
		break;
	case (PRINTLN):
		print(p_table + index, true); // Add newline
		break;

	// Terminating
	case (STOP):
		setProcessState(p_table[index].pid, 't');
		return true; // Terminate process
	default:
		Serial.print(F("unknown process instruction: "));
		Serial.println(nextInstruction, DEC);
		setProcessState(p_table[index].pid, 't');
		return true; // Terminate process
	}

	return false; // Program still running (found no STOP)
}
