#ifndef ARDUTRY3_H
#define ARDUTRY3_H

#include "stack.h"
#include "memory.h"
#include "process.h"
#include <EEPROM.h>

#define BUFFSIZE 12 // Size of the buffer
#define BUFF_ARG_SIZE 60

#define nameSize 12
#define parameterSize 10
#define descriptSize 30

#define FILENAME_SIZE 12 // Size file name
#define FAT_START 1
#define FAT_SIZE_MAX 10 // 10 FAT positions only, each one 16 bytes

//==============================================================================
//== Structs

// Defines a function using a struct
typedef struct {
  char name[BUFFSIZE];
  void (*func)();
}commandType;

typedef struct{
  char name[nameSize];
  char parameter[parameterSize];
  char descr[descriptSize];
}commandList;

// EEPROM struct
typedef struct{
  char filename[FILENAME_SIZE];
  int filePosition;
  int fileSize;
}eepromfile;

//==============================================================================
//== Beschikbare commando's in de commandline interface
void store();       // sla een bestand op in het bestandssysteem
void retrieve();    // vraag een bestand op uit het bestandssysteem
void erase();       // wis een bestand
void files();       // print een lijst van bestanden
void freespace();   // print de beschikbare ruimte in het bestandssysteem
void run();         // start een programma
void list();        // print een lijst van processen
void suspend();     // pauzeer een proces
void resume();      // hervat een proces
void kill();        // kill stop een proces
void help();        // print lijst met bestaande commandos

void callFunction();
void buffer();
void printCommandList();
void checkForCommand();
bool writeCommand(char bufferReceived);
int writeArgument(int argumentNumber, char bufferReceived);
void emptyBuffer();

// EEPROM Functions
bool writeFATEntry(byte position, eepromfile file);
eepromfile readFATEntry(byte position);
int storeFile(char* name, int size, byte* data);
int checkFATEntry(char* name);
void writeData(int startPosition, int size, byte* data);
int findFreeSpace(int size);
int getStartPos(int currentIndex);
int getNextFileStartPos(int index);
int getFileSize(int index);
int indexZeroFile();
void formatFAT();
void writeTestPrograms();

//==============================================================================
//== Lists

// Places a command in a array
static commandType command[] = {
  {"store",         &store},
  {"retrieve",      &retrieve},
  {"erase",         &erase},
  {"files",         &files},
  {"freespace",     &freespace},
  {"run",           &run},
  {"list",          &list},
  {"suspend",       &suspend},
  {"resume",        &resume},
  {"kill",          &kill},
  {"help",          &help},
  {"format",        &formatFAT}
};

static commandList infoList[] = {
  {"store", "file,size,data", "Store files in filesystem"},
  {"retrieve", "file", "Retrieve file from filesystem"},
  {"erase", "file", "Erase file from filesystem"},
  {"files", "", "Lists files present in filesystem"},
  {"freespace", "", "Free space in filesystem"},
  {"run", "", "Run program"},
  {"list", "", "List running processes"},
  {"suspend", "id", "Suspend process"},
  {"resume", "id", "Resume process"},
  {"kill", "id", "Kill process"},
  {"help", "", "list the available commands"},
  {"format", "", "format files in EEPROM"}
};

//==============================================================================



#endif