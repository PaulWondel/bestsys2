/*
 * Stack Manager
 */

#include "stack.h"
#include "instruction_set.h"

//==============================================================================
// Bytes
// Credit: Thomas Graafland
void pushByte(stackType* stack, byte b)
{
    stack->stack[(stack->sp)++] = b; // Push byte onto stack and increment SP
}
byte popByte(stackType* stack, bool peek)
{
    if (peek)
        return stack->stack[(stack->sp)-1]; // Return without chaning SP
    return stack->stack[--(stack->sp)]; // Return and lower SP
}

//==============================================================================
// Integers
void pushInt(stackType* stack, int i)
{
    pushByte(stack, highByte(i)); // Left byte
    pushByte(stack, lowByte(i)); // Right byte
    pushByte(stack, INT); // Type
}
int popInt(stackType* stack)
{
    popByte(stack); // Type
    byte right = popByte(stack); // Right byte
    byte left = popByte(stack); // Left byte
    return word(left, right); // Return int
}

//==============================================================================
// Floats
// Credit: Thomas Graafland
void pushFloat(stackType* stack, float f)
{
    byte* b = (byte*) &f; // Convert to byte array

    for (int_least8_t i = 3; i >= 0; i--)
        pushByte(stack, b[i]); // Push byte array

    pushByte(stack, FLOAT); // Type
}
float popFloat(stackType* stack)
{
    popByte(stack); // Type
    float f = 0.0;
    byte* b = (byte*) &f; // Create byte array

    for (int_least8_t x = 0; x < FLOAT; x++)
        b[x] = popByte(stack); // Fill byte array

    return f; // Return float
}

//==============================================================================
// Chars
void pushChar(stackType* stack, char c){
    pushByte(stack, c); // Push char as byte
    pushByte(stack, CHAR); // Type
}
char popChar(stackType* stack)
{
    popByte(stack); // Type
    return (char) popByte(stack); // Return char
}

//==============================================================================
// Strings
void pushString(stackType* stack, char* str, uint_least8_t length)
{
    for (uint_least8_t x = 0; x < length; x++)
        pushByte(stack, str[x]); // Push each char in string

    pushByte(stack, length); // length of the string with terminating zero
    pushByte(stack, STRING); // Type
}
char* popString(stackType* stack)
{
    // Credit: Thomas Graafland
    popByte(stack); // Type
    uint_least8_t length = popByte(stack); // Length of string
    stack->sp -= length; // Lower stack pointer by length
    return (stack->stack + stack->sp); // Return start address of string
}

//==============================================================================
// Pops a value from stack and returns it as float
float popVal(stackType* stack, bool peek)
{
    float f;
    byte type = popByte(stack, true); // Peek type

    switch (type)
    {
        case (INT):
            f = (float) popInt(stack);
            if (peek) pushInt(stack, (int) f); // Push back onto stack
            break;
        case (CHAR):
            f = (float) popChar(stack);
            if (peek) pushChar(stack, (char) f); // Push back onto stack
            break;
        case (FLOAT):
            f = popFloat(stack);
            if (peek) pushFloat(stack, f); // Push back onto stack
    }

    return f; // Always return as float
}
