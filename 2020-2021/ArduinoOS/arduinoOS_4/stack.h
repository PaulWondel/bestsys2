#ifndef STACK_H
#define STACK_H

#include "instruction_set.h"
#include <Arduino.h>

#define STACKSIZE 32

typedef struct 
{
    byte stack[STACKSIZE];
    uint_least8_t sp; 
}stackType;


void pushByte(stackType* stack, byte b);
byte popByte(stackType* stack, bool peek = false);

void pushChar(stackType* stack, char c);
char popChar(stackType* stack);

void pushInt(stackType* stack, int i);
int popInt(stackType* stack);

void pushFloat(stackType* stack, float f);
float popFloat(stackType* stack);

void pushString(stackType* stack, char *str, uint_least8_t length);
char* popString(stackType* stack);

float popVal(stackType* stack, bool peek=false);

#endif
