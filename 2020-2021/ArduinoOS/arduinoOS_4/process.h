#ifndef PROCESS_H
#define PROCESS_H    

#include <Arduino.h>
#include "stack.h"
#include "ardutry3.h"
#include "instruction_set.h"

#define MAXFILENAMESIZE 12
#define MAXPROCESSES 10
#define RUNPROCESSTIME 50 // Time in millis for each program cycle

typedef struct
{
    char name[MAXFILENAMESIZE];
    char state; // running(r) paused(p) terminated(t)
    byte pid; // process id
    int pc; // program pointer
    int fp; // file pointer
    int sp; // stack pointer
    int loop;
    stackType stack;
}process;

// Variables
byte start(char* fileName);

// Functions prototypes
void printProcess(byte pid);
void listProcesses();
int processStart(char* fileName);
byte getNumOfRunningProgramms();

void kill(int pid);
bool checkProcessExists(int pid);
bool setProcessState(int index, char state);
bool execute(int pid);
void runPrograms();

#endif
