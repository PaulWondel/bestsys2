/*
ArduinOS 2.0
Besturingssystemen 2
TINBES02-2
Hogeschool Rotterdam
 
Gemaakt door:
- Paul Wondel        (0947421@hr.nl)
- Rowalski Wever     (0918221@hr.nl)
- Jeffrey Brons      (0950733@hr.nl)
*/

#include "commandLine.h"

void setup() {
    Serial.begin(9600);
    Serial.println("<Welcome to Arduino OS 2.0!>");
    commandLine::startCommandLine();
}

void loop()
{
  commandLine::inputBuffer();
}