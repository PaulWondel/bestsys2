#ifndef COMMANDLINE_H
#define COMMANDLINE_H

const int BUFFSIZE = 12; // Size of the buffer

namespace commandLine
{
  //==============================================================================
  // Variabelen, structs etc
  typedef struct {
    char name [BUFFSIZE];
    void *func;
  } commandType;
  bool incomingData = false;        // check voor serial input
  bool firstCommand = false;         // eerste commando
  int currentArgNumber = 0;         // argument teller
  bool existingCommand = false;     // check voor bestaand commando

  char* commandBuffer = new char[BUFFSIZE];           // buffer voor commando
  char** argumentBuffer = new char*[3];      // buffer voor argumenten, 3 arrays in 1 array dus double pointer

  // extern bool incomingData;        // check voor serial input
  // extern bool firstCommand;         // eerste commando
  // extern int currentArgNumber;         // argument teller
  // extern bool existingCommand;     // check voor bestaand commando

  // Buffer variabelen
  // extern char* commandBuffer;          // buffer voor commando
  // extern char** argumentBuffer;     // buffer voor argumenten, 3 arrays in 1 array dus double pointer
  

  //==============================================================================
  // Beschikbare commando's in de commandline interface
  void store();       // sla een bestand op in het bestandssysteem
  void retrieve();    // vraag een bestand op uit het bestandssysteem
  void erase();       // wis een bestand
  void files();       // print een lijst van bestanden
  void freespace();   // print de beschikbare ruimte in het bestandssysteem
  void run();         // start een programma
  void list();        // print een lijst van processen
  void suspend();     // pauzeer een proces
  void resume();      // hervat een proces
  void kill();        // kill stop een proces
  void help();

  // Pointers naar de commando's
  static commandType command[] = {
    {"store",         &store},
    {"retrieve",      &retrieve},
    {"erase",         &erase},
    {"files",         &files},
    {"freespace",     &freespace},
    {"run",           &run},
    {"list",          &list},
    {"suspend",       &suspend},
    {"resume",        &resume},
    {"kill",          &kill},
    {"help",          &help}
  };

  //==============================================================================
  // Serial monitor reader functies
  void inputBuffer();                                                          // plaats ingevoerde tekst van serial plaatsen in buffer
  char* addToBuffer(char* addChar, char receivedChar, int MAX_BUFF_SIZE);      // buffer lezen
  void runCommando();                                                          // commando uit buffer executen
  bool writeCommand(char bufferReceived);
  bool writeArgument(char bufferReceived);

  void printBuffer();
  void startCommandLine();
}


#endif