#include "Arduino.h"
#include "commandLine.h"

// bool incomingData = false;        // check voor serial input
// bool firstCommand = false;         // eerste commando
// int currentArgNumber = 0;         // argument teller
// bool existingCommand = false;     // check voor bestaand commando

// char* commandBuffer = new char[BUFFSIZE];           // buffer voor commando
// char** argumentBuffer = new char*[3];      // buffer voor argumenten, 3 arrays in 1 array dus double pointer

//==============================================================================
void commandLine::startCommandLine(){

  commandBuffer[0] = '\0';
  for(int i = 0; i < 2; i++)
  {
    argumentBuffer[i] = new char[BUFFSIZE];
    argumentBuffer[i][0] = '\0';
  }
  currentArgNumber = 0;
  incomingData = false;
}

// Serial monitor reader functies
void commandLine::inputBuffer(){
  char received;

  while (Serial.available() > 0 && incomingData == false)
  {
    received = Serial.read();
    if (received != '\n')
    {
      if (!firstCommand)
      {
        firstCommand = writeCommand(received);
      }
      else {
        currentArgNumber += writeArgument(received);
      }
      return;
    }
    incomingData = true;
    printBuffer();
    return;
  }
}

char* commandLine::addToBuffer(char* addCharToBuffer, char receivedChar, int MAX_BUFF_SIZE){
  int currentSize = strlen(addCharToBuffer);
  if (currentSize == (MAX_BUFF_SIZE - 1))
  {
    return addCharToBuffer;
  }

  addCharToBuffer[currentSize] = receivedChar;
  addCharToBuffer[receivedChar + 1] = 0;
  return addCharToBuffer;
}

bool commandLine::writeCommand(char bufferReceived){
  if (bufferReceived == ' ' or bufferReceived == '\n'){
    return true;
  }
  commandBuffer = addToBuffer(commandBuffer, bufferReceived, BUFFSIZE);
  return false;
}

bool commandLine::writeArgument(char bufferReceived){
  if (bufferReceived == ' ' or bufferReceived == '\n'){
    return true;
  }
  argumentBuffer[currentArgNumber] = addToBuffer(argumentBuffer[currentArgNumber], bufferReceived, 3);
  return false;
}

void commandLine::runCommando(){
  Serial.println("Run command with variable received");
}

void commandLine::printBuffer(){
  Serial.print(F("$ "));
  Serial.print(commandBuffer);
  for (byte i = 0; i < sizeof(argumentBuffer) + 1; i++)
  {
    Serial.print(F(" "));
    Serial.print(argumentBuffer[i]);
  }
  Serial.println();
}

//==============================================================================
// Functies van de commandolijst
void commandLine::store(){
  Serial.println("This is the store function");
}

void commandLine::retrieve(){
  Serial.println("This is the retrieve function");
}

void commandLine::erase(){
  Serial.println("This is the erase function");
}

void commandLine::files(){
  Serial.println("This is the files function");
}

void commandLine::freespace(){
  Serial.println("This is the freespace function");
}

void commandLine::run(){
  Serial.println("This is the run function");
}

void commandLine::list(){
  Serial.println("This is the list function");
}

void commandLine::suspend(){
  Serial.println("This is the suspend function");
}

void commandLine::resume(){
  Serial.println("This is the resume function");
}

void commandLine::kill(){
  Serial.println("This is the kill function");
}

void commandLine::help(){
  Serial.println("The following commands are available:");
  for (int i = 0; i < sizeof(command) / sizeof(commandType); i++) {
    Serial.print("- ");
    Serial.println(command[i].name);
  }
}