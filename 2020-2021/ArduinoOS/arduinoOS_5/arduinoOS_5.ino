/*
ArduinOS 5.0
Besturingssystemen 2
TINBES02-2
Hogeschool Rotterdam
 
Gemaakt door:
- Rowalski Wever     (0918221@hr.nl)
- Paul Wondel        (0947421@hr.nl)
*/

#include "ardutry3.h"
#include "test_programs.h"

// Buffer variables
static int commandSize = sizeof(command) / sizeof(commandType);
bool incomingData = false;    // check for serial input
bool firstCommand = false;    // first command checker
int currentArgNumber = 0;     // argument counter
bool existingCommand = false; // check for existing commando

char commandBuffer[BUFFSIZE]; // buffer for the commands
char arg1[BUFFSIZE];          // buffer first argument
char arg2[BUFFSIZE];          // buffer second argument
char arg3[BUFFSIZE];          // buffer third argument

// FAT variables
EERef noOfFiles = EEPROM[0]; // If put as EEPROM[160] we get memory leak

void setup()
{
  Serial.begin(9600);
  Serial.print(F("\n"));
  Serial.print(F("<Welcome to Arduino OS v5! Final Build>\n"));
  Serial.print(F("<Using buffer 2.0!>\n"));
  addTestPrograms(); // add test programs to FAT storage
}

void loop()
{
  buffer();
  runPrograms();
  delayMicroseconds(1024);
}

//==============================================================================
//== Command line

// Serial buffer from the input
void buffer()
{
  char answer;

  //only runs when data is coming in
  while (Serial.available() > 0 && incomingData == false)
  {
    answer = Serial.read();
    if (answer != '\n')
    {
      if (firstCommand != true)
      {
        firstCommand = writeCommand(answer);
      }
      else
      {
        currentArgNumber = writeArgument(currentArgNumber, answer);
      }
      return;
    }
    incomingData = true;
    checkForCommand();
    incomingData = false;
    // printInputData();
    emptyBuffer(); //clear buffer after loop finished
    return;
  }
}
//check if it is a valid command
//if valid stop the while loop
void checkForCommand()
{
  if (incomingData)
  {
    for (int i = 0; i < commandSize; i++)
    {
      if (strcmp(commandBuffer, command[i].name) == 0)
      {
        void (*func)() = command[i].func;
        func();
        existingCommand = true;
        return;
      }
    }
    if (existingCommand)
    {
      incomingData = false;
      existingCommand = false;
      return;
    }
    else
    {
      Serial.print(F("Command unknown. Type 'help' to see commands."));
    }
    incomingData = false;
  }
}

//==============================================================================
//== Writing helper functions
//if there is a space or new line, add everything before the space to command buffer
bool writeCommand(char bufferReceived)
{
  if (bufferReceived == ' ' || bufferReceived == '\n')
  {
    return true;
  }

  int bufferSize = strlen(commandBuffer);

  if (bufferSize >= BUFFSIZE)
  {
    commandBuffer[BUFFSIZE - 1] = bufferReceived;
    return false;
  }
  commandBuffer[bufferSize] = bufferReceived;
  return false;
}
//if there is a space or new line, add everything before the space to argument(s) buffer
int writeArgument(int argumentNumber, char bufferReceived)
{
  if (bufferReceived == ' ' || bufferReceived == '\n')
  {
    argumentNumber += 1;
    return argumentNumber;
  }
  if (currentArgNumber == 0)
  {
    int bufferSize = strlen(arg1);

    if (bufferSize >= BUFFSIZE)
    {
      arg1[BUFFSIZE - 1] = bufferReceived;
      return argumentNumber;
    }
    arg1[bufferSize] = bufferReceived;
    return argumentNumber;
  }
  else if (currentArgNumber == 1)
  {
    int bufferSize = strlen(arg2);

    if (bufferSize >= BUFFSIZE)
    {
      arg2[BUFFSIZE - 1] = bufferReceived;
      return argumentNumber;
    }
    arg2[bufferSize] = bufferReceived;
    return argumentNumber;
  }
  else if (currentArgNumber == 2)
  {
    int bufferSize = strlen(arg3);

    if (bufferSize >= BUFFSIZE)
    {
      arg3[BUFFSIZE - 1] = bufferReceived;
      return argumentNumber;
    }
    arg3[bufferSize] = bufferReceived;
    return argumentNumber;
  }
  return argumentNumber;
}

//==============================================================================
//== Empty the buffer

// Empty the buffer by making all buffers '\0'
void emptyBuffer()
{
  for (int i = 0; i < sizeof(commandBuffer); i++)
  {
    commandBuffer[i] = '\0';
  }
  for (int i = 0; i < sizeof(arg1); i++)
  {
    arg1[i] = '\0';
  }
  for (int i = 0; i < sizeof(arg2); i++)
  {
    arg2[i] = '\0';
  }
  for (int i = 0; i < sizeof(arg3); i++)
  {
    arg3[i] = '\0';
  }
  currentArgNumber = 0;
  firstCommand = false;
}

//==============================================================================
//== Print functions

// Print Contents of the all buffers
// void printInputData()
// {
//   if (incomingData)
//   {
//     Serial.print(F("Previous input: "));
//     Serial.print(commandBuffer);
//     Serial.print(F(" "));
//     Serial.print(arg1);
//     Serial.print(F(" "));
//     Serial.print(arg2);
//     Serial.print(F(" "));
//     Serial.println(arg3);

//     incomingData = false;
//   }
// }

//==============================================================================
//== Commando's Functions

// store a file in FAT with contents
void store()
{
  int filesize = atoi(arg2) + 1;
  int errorCode = storeFile(arg1, filesize, (byte *)arg3);
  switch (errorCode) // Error code
  {
  case (1):
    Serial.print(F("stored: "));
    Serial.println(arg1);
    break;
  case (-1):
    Serial.println(F("filename too long"));
    break;
  case (-2):
    Serial.println(F("insufficient space"));
    break;
  case (-3):
    Serial.println(F("filename already exists"));
  }
}

// read contents of file stored in FAT
void retrieve()
{
  int index = checkFATEntry(arg1);

  if (index == -1)
  {
    Serial.println(F("File not found!")); // File does not exist
    return;
  }

  int startPos = getStartPos(index); // Get start address
  int size = getFileSize(index);     // Get file size

  char byteFATData[size]; // byte array for bytes

  for (int i = 0; i < size; i++)
  {
    byteFATData[i] = EEPROM[startPos + i], DEC;
  }
  Serial.println(byteFATData);
}

// erase stored file in FAT
void erase()
{
  int location = checkFATEntry(arg1);
  if (location != -1)
  {
    eepromfile file = readFATEntry(location);
    file.filename[0] = '\0'; // Clear filename
    file.filePosition = 0;
    file.fileSize = 0;
    writeFATEntry(location, file); // Write empty FAT entry
    noOfFiles -= 1;                // file counter -1
  }
  Serial.print(F("File deleted\n"));
}

// list files stored in FAT
void files()
{
  eepromfile file;
  for (byte i = 0; i < FAT_SIZE_MAX; i++) // Go through all files in FAT
  {
    file = readFATEntry(i);
    if (file.fileSize > 0) // Only print files that contain data
    {
      Serial.print(F("file: "));
      Serial.print(i); // File index from FAT
      Serial.print(F("\t- "));
      Serial.print(file.filename); // Filename
      Serial.print(F("\t\t("));
      Serial.print(file.filePosition); // byte on disk
      Serial.print(F("/"));
      Serial.print(file.filePosition + file.fileSize - 1); // End byte on disk
      Serial.print(F(" - "));
      Serial.print(file.fileSize); // Filesize
      Serial.println(F(" bytes)"));
    }
    else
    {
      Serial.println(F("No file saved"));
    }
  }
}

//prints the amount of space available
void freespace()
{
  int freeSpace = EEPROM.length(); // Get EEPROM size

  freeSpace -= sizeof(noOfFiles);                   // Substract noOfFiles
  freeSpace -= (sizeof(eepromfile) * FAT_SIZE_MAX); // Substract FAT size

  for (byte i = 0; i < FAT_SIZE_MAX; i++) // Go trough all files in FAT
  {
    eepromfile file = readFATEntry(i);
    if (file.fileSize != 0)
      freeSpace -= file.fileSize;
  }
  Serial.print(F("freeSpace: "));
  Serial.print(freeSpace);
  Serial.print(F(" bytes\n"));
}

// execute program stored in FAT
void run()
{
  int result = processStart(arg1); // Try to start a process
  switch (result)                  // Print the result
  {
  case (1):
    Serial.print(F("started: "));
    Serial.println(arg1);
    break;
  case (-1):
    Serial.println(F("too many processes"));
    break;
  case (-2):
    Serial.println(F("file does not exist"));
  }
}

// list current running processes
void list()
{
  listProcesses();
}

// suspend a running process
void suspend()
{
  Serial.print(F("Suspend: "));
  Serial.println(arg1);
  setProcessState(atoi(arg1), 'p');
}

//resume current paused process
void resume()
{
  Serial.print(F("Resume: "));
  Serial.println(arg1);
  setProcessState(atoi(arg1), 'r');
}

// terminate a running or suspended process
void kill()
{
  Serial.print(F("Kill pid: "));
  Serial.println(arg1);
  setProcessState(atoi(arg1), 't');
}

// list the existing commando's that are available
void help()
{
  Serial.println(F("The following commands are available:"));
  for (int i = 0; i < commandSize; i++)
  {
    Serial.print("- ");
    Serial.println(command[i].name);
  }
}

//==============================================================================
//== FAT Function

// write file to FAT
bool writeFATEntry(byte position, eepromfile file)
{
  EEPROM.put(FAT_START + position * sizeof(eepromfile), file);
  return;
}

// read file from FAT
eepromfile readFATEntry(byte position)
{
  eepromfile file;
  EEPROM.get(FAT_START + position * sizeof(eepromfile), file);
  return file;
}

// create file and allocate size
int storeFile(char *name, int size, byte *data)
{
  if (sizeof(name) > FILENAME_SIZE)
  {
    return -1;
  }
  if (noOfFiles == FAT_SIZE_MAX) // Check FAT size
    return -2;
  if (checkFATEntry(name) != -1) // Check if name does not exist
    return -3;

  int position = findFreeSpace(size);
  if (position == -1)
  {
    return -2; //no space available
  }

  eepromfile file = (eepromfile){"", position, size};
  strcpy(file.filename, name);
  writeFATEntry(indexZeroFile(), file); // Store file in FAT
  writeData(file.filePosition, size, data);

  noOfFiles += 1; // 1 eternity later, a file added
  return 1;
}

// check if filename already exists in FAT
int checkFATEntry(char *name)
{
  for (byte i = 0; i < FAT_SIZE_MAX; i++) // Go through all files in FAT
  {
    eepromfile file = readFATEntry(i);
    if (strcmp(file.filename, name) == 0)
      return i; // Return entry ID
  }
  return -1; // Name does not exist
}

//store the data in EEPROM
void writeData(int startPosition, int size, byte *data)
{
  int nextAddress = 0;
  for (int i = 0; i < size; i++) // Store all bytes to disk
  {
    EEPROM.write(startPosition + nextAddress, data[i]); // Store byte on EERPOM
    nextAddress += sizeof(data[i]);                     // Skip to next address
  }
}

// find how much space is free for use
int findFreeSpace(int size)
{
  // Get first free byte
  // noOfFiles + FAT size + padding byte
  int firstFreeByte = sizeof(noOfFiles) + (sizeof(eepromfile) * FAT_SIZE_MAX) + 1;

  if (noOfFiles == 0)
    return firstFreeByte; // Write first file byte after FAT

  // Check space between files or space untill end of EEPROM
  bool firstFile = true;
  for (byte i = 0; i < FAT_SIZE_MAX; i++) // Already checked 0, so start at 1
  {
    eepromfile file = readFATEntry(i); // Get file
    if (file.fileSize > 0)             // Check if file has data
    {
      // Check the first file in FAT
      if (firstFile && (file.filePosition - firstFreeByte > size))
        return firstFreeByte; // Write data after FAT
      firstFile = false;

      // Check if there is space between current file and next
      //  file (or EERPOM end if no more files exist)
      if (size < getNextFilePosition(i) - (file.filePosition + file.fileSize))
        return (file.filePosition + file.fileSize); // Write after file
    }
  }
  return -1; // space not enough
}

// Get startPos from file index
int getStartPos(int currentIndex)
{
  eepromfile file = readFATEntry(currentIndex);
  return file.filePosition; // Return the start postion on EEPROM
}

// get the next position after the last stored file
int getNextFilePosition(int index)
{
  for (byte n = index + 1; n < FAT_SIZE_MAX; n++) // Go through all files in FAT
  {
    eepromfile file = readFATEntry(n);
    if (file.fileSize != 0)
      return file.filePosition;
  }
  return (EEPROM.length() + 1); // Add one so we can use last byte of EEPROM
}

// Get file size from file index
int getFileSize(int index)
{
  eepromfile file = readFATEntry(index);
  return file.fileSize; // Return file size
}

//finds first free file location in FAT
int indexZeroFile()
{
  for (byte i = 0; i < FAT_SIZE_MAX; i++) // Go through all files in FAT
  {
    eepromfile file = readFATEntry(i);
    if (file.fileSize == 0)
      return i; // Return first free file
  }
  return -1;
}

//delete all files saved in EEPROM
void formatFAT()
{
  noOfFiles = 0;
  for (byte i = 0; i < FAT_SIZE_MAX; i++)
  {
    eepromfile emptyFile = (eepromfile){"", 0, 0};
    writeFATEntry(i, emptyFile);
  }
}

//==============================================================================
//== Stack and Memory functions

// add test programs for running processes and testing variable exchanges in the stack
void addTestPrograms()
{
  storeFile("hello_world", sizeof(prog1), prog1);
  storeFile("test_vars", sizeof(prog2), prog2);
  storeFile("test_loop", sizeof(prog3), prog3);
  storeFile("test_if", sizeof(prog4), prog4);
  // storeFile("test_while", sizeof(prog5), prog5);
  // storeFile("blink", sizeof(prog6), prog6);
  // storeFile("read_file", sizeof(prog7), prog7);
  // storeFile("write_file", sizeof(prog8), prog8);
  // storeFile("test_fork", sizeof(prog9), prog9);
}