#pragma once

#include <Arduino.h>
#include "process.h"


// Values
void valueToStack(process *process, byte type);

// Unary operations
void unaryOperation(process *process, byte operation);

// Binary operations
void binaryOperation(process *process, byte operation);

// Arduino operations
void arduinoOperation(process *process, byte operation);

// Time operations
void timeOperation(process *process, byte operation);

// Branch operations
void branchOperation(process *process, byte operation);

// Printing
void print(process *process, bool newline = false);
