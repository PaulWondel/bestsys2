#ifndef ARDUTRY3_H
#define ARDUTRY3_H

#include "stack.h"
#include "memory.h"
#include "process.h"
#include <EEPROM.h>

#define BUFFSIZE 12 // Size of the buffer
#define BUFF_ARG_SIZE 60

#define FILENAME_SIZE 12 // Size file name
#define FAT_START 1
#define FAT_SIZE_MAX 10 // 10 FAT positions only, each one 16 bytes

//==============================================================================
//== Structs

// Defines a function using a struct
typedef struct {
  char name[BUFFSIZE];
  void (*func)();
}commandType;

// EEPROM struct
typedef struct{
  char filename[FILENAME_SIZE];
  int filePosition;
  int fileSize;
}eepromfile;

//==============================================================================
//== Beschikbare commando's in de commandline interface
void store();       // save a file in the file system
void retrieve();    // retrieve a file from FAT and read contents
void erase();       // delete a file
void files();       // print current store files 
void freespace();   // print the free space available
void run();         // start a program
void list();        // prints a list of all processes
void suspend();     // pause process
void resume();      // resume a paused process
void kill();        // kill a running or suspended process
void help();        // print list with all available commands
void formatFAT();   // format the FAT storage

// CommandLine Functions (Buffer 2.0)
// void buffer();
// void checkForCommand();
// bool writeCommand(char bufferReceived);
// int writeArgument(int argumentNumber, char bufferReceived);
// void emptyBuffer();
// void printInputData();

// EEPROM Functions
bool writeFATEntry(byte position, eepromfile file);
eepromfile readFATEntry(byte position);
int storeFile(char* name, int size, byte* data);
int checkFATEntry(char* name);
void writeData(int startPosition, int size, byte* data);
int findFreeSpace(int size);
int getStartPos(int currentIndex);
int getNextFileStartPos(int index);
int getFileSize(int index);
int indexZeroFile();

//==============================================================================
//== Lists

// All existing commands in pointer array for usage
static commandType command[] = {
  {"store",         &store},
  {"retrieve",      &retrieve},
  {"erase",         &erase},
  {"files",         &files},
  {"freespace",     &freespace},
  {"run",           &run},
  {"list",          &list},
  {"suspend",       &suspend},
  {"resume",        &resume},
  {"kill",          &kill},
  {"help",          &help},
  {"format",        &formatFAT}
};

//==============================================================================



#endif