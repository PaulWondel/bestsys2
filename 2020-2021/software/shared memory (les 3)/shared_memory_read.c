#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
int main() {
   key_t key = ftok("shm", 1);
   int shmid = shmget(key, 1024, 0666);
   char *str = shmat(shmid, 0, 0);
   printf("Data read from memory: %s\n", str);
   shmdt(str);
   shmctl(shmid, IPC_RMID, NULL);
   return 0;
}
