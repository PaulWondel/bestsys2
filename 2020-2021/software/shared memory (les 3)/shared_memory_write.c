#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
int main() {
   key_t key = ftok("shm", 1);
   int shmid = shmget(key, 1024, 0666 | IPC_CREAT);
   char *str = shmat(shmid, 0, 0);
   puts("Enter data: ");
   gets(str);
   printf("Data written in memory: %s\n", str);
   shmdt(str);
   return 0;
}
