#ifndef instructions_h
#define instructions_h
#include "Arduino.h"
#include "processes.h"
#include "fat.h"
#include "memory.h"
#include "stack.h"
#include "instructions.h"
#include <EEPROM.h>


class instructionclass
{
  public:
  	
  	instructionclass();
    void runProcesses();
    void execute(int indexProces);
};

extern instructionclass instructions;

#endif