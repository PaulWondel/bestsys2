#include "fat.h"
#include "memory.h"
#include <EEPROM.h>
#include "Arduino.h"
#include "instructions.h"
#include "processes.h"
#include "instruction_set.h"
#include "test_programs.h"

char Buffer[12];
Program ProgramTable[10];

//functions
void store();
void erase();
void retrieve();
void files();
void readBuffer();
void freespace();
void run_();
void list();
void suspend();
void resume_();
void kill();

//type definition of a command
typedef struct {
  char Name[10];
  void * Func;
} commandType;

//array of command types
static commandType command [] = {
  {"store", &store} ,
  {"retrieve", &retrieve },
  {"erase", &erase },
  {"files", &files },
  {"freespace", &freespace },
  {"run", &run_ },
  {"list", &list },
  {"suspend", &suspend },
  {"resume", &resume_ },
  {"kill", &kill }
};

void setup() {
  for (int i = 0 ; i < EEPROM.length() ; i++) {
    EEPROM.write(i, 0);
  }
  Serial.begin(9600);
  fat.SETUP();
  Serial.println(F("Welcome: "));
  fat.store("prog", 64, prog4);

}



void loop() {
  delay(1);
  readInput();
  readBuffer();
  clearReceived();
  instructions.runProcesses();
}

void clearReceived() {
  for (int i = 0; i < sizeof(Buffer); i++) {
    Buffer[i] = '\0';
  }
}

void readInput() {
  int i = 0;
  delay(1);
  while (Serial.available() > 0)
  {
    Buffer[i] = Serial.read();
    i++;
    delay(10);
  }

  //Serial.println("readInput: ");
  //Serial.println(Buffer);
}


void readBuffer()
{
  //return if Buffer is empty
  if (Buffer[0] ==  '\0')
  {
    return;
  }
  for (int i = 0; i < sizeof(command) / sizeof(commandType); i++) {
    if (strcmp(Buffer , command[i].Name) == 0) {
      void (*func)() = command[i].Func;
      clearReceived();
      Serial.readString();
      func();
      Serial.readString();
      return;
    }
  }
  Serial.println("Command not found");
  for (int i = 0; i < sizeof(command) / sizeof(commandType); i++) {
    Serial.println(command[i].Name);
  }
}


void store()
{
  Serial.println("Name");
  readInput();
  char Name[12];
  memcpy(Name, Buffer, 12);
  clearReceived();
  Serial.println(sizeof(Name));
  Serial.println("data");
  readInput();
  char Data[12];
  memcpy(Data, Buffer, 12);
  clearReceived();
  Serial.println("Length");
  readInput();
  fat.store(Name, atoi(Buffer), Data);
  clearReceived();
}
void retrieve()
{
  Serial.println("name");
  while (Buffer[0] ==  '\0')
  {
    readInput();
    delay(10);
  }
  fat.retrieve(Buffer);
}
void erase()
{
  Serial.println("name");
  readInput();
  fat.erase(Buffer);
  clearReceived();
}
void freespace()
{
  fat.freespace();
}
void files()
{
  fat.files();
}
void run_()
{
  Serial.println("name");
  while (Buffer[0] ==  '\0')
  {
    readInput();
    delay(10);
  }
  Serial.println(Buffer);
  delay(10);
  processes.runProgram(Buffer);
}
void list()
{
  processes.listPrograms();
}
void suspend()
{
  int id;
  Serial.println(F("ID"));
  readInput();
  id = atoi(Buffer);
  clearReceived();
  processes.suspend(id);
}
void resume_()
{
  int id;
  Serial.println(F("ID"));
  readInput();
  id = atoi(Buffer);
  clearReceived();
  processes.resume(id);
}
void kill()
{
  int id;
  Serial.println(F("enter ID"));
  while (Buffer[0] ==  '\0')
  {
    readInput();
  }
  id = atoi(Buffer);
  processes.kill(id);
}
