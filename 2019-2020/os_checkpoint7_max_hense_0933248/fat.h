#ifndef fan_h
#define fan_h
#include "Arduino.h"


typedef struct {
  char Name[12];
  int Start;
  int Length;
} fileEntry;

class fclass
{
  public:
    fclass();
    fileEntry;
    int findFile(char Name[12]);
    void SETUP();
    fileEntry retrieve(char Name[12]);
    void fatSort();
    void fatSwap(fileEntry Entry1, int Pos1, fileEntry Entry2, int Pos2);
    void erase(char Name[12]);
    void writeFATEntry(char Name[12], int Length, int Pos);
    void files();
    void store(char Name[12], int Length, char data[12]);
    void writeCharArrayToEEPROM(char input[], int locatie, int length);
    void freespace();
};

extern fclass fat;

#endif
