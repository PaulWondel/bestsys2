#ifndef processes_h
#define processes_h
#include "Arduino.h"
#include  "instruction_set.h"


typedef struct{
  char Name[12];
  int ProcessID;
  byte Status;
  int ProgramCounter;
  int FilePointer;
  int StackPointer;
  int LoopStart;
  byte stack_[32];
} Program; 


extern Program ProgramTable[];

class processclass
{
  public:
    processclass();
    Program;
    //ProgramTable[10];
    void SETUP();
    void runProgram(char Name[12]);
    void suspend(int ProcessID);
    int checkForProgram(int ProcessID);
    void changeStatus(int ProcessID, byte Status);
    void resume(int ProcessID);
    void kill(int ProcessID);
    void listPrograms();
};

extern processclass processes;
#endif
