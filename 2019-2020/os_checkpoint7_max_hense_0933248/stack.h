#ifndef stack_h
#define stack_h
#include "Arduino.h"



class stackclass
{
  public:
    stackclass();
    void SETUP();
    void pushByte(byte b, int i);
    byte popByte(int i);
    byte Peek(int proces);
    float popVal(int proces);
    float popFloat(int proces);
    int popInt(int proces);
    void popString(int proces);
    void pushString(char string[12], int proces);
    void pushFloat(float kommaGetal, int proces);
    void pushInt(int getal, int proces);
};

extern stackclass stack;
#endif
