#include "Arduino.h"
#include "processes.h"
#include "fat.h"
#include "memory.h"

processclass::processclass(){}

int Programs;

void processclass::SETUP()
{

}

void processclass::runProgram(char Name[12])
{
  // check if there is space in the program table
  if(Programs >=10)
  {
    Serial.println("Maximum amount of processes reached!");
    return;
  }
  if(fat.findFile(Name) == -1)
  {
    Serial.println("Program does not exist!");
    return;
  }
  fileEntry RetrievedProgram = fat.retrieve(Name);
  //Serial.println(RetrievedProgram.Name);
  ProgramTable[Programs].Name = RetrievedProgram.Name;
  memcpy(ProgramTable[Programs].Name, RetrievedProgram.Name, 12);
  ProgramTable[Programs].ProcessID = Programs;
  ProgramTable[Programs].ProgramCounter = RetrievedProgram.Start;
  ProgramTable[Programs].StackPointer = 0;
  ProgramTable[Programs].Status = 'r';
  //Serial.println(ProgramTable[0].Name);
  Programs++;
  Serial.println("Program started");

}

void processclass::suspend(int ProcessID)
{
  if(checkForProgram(ProcessID) == -1)
  {
    Serial.println("terminated");
    return;
  }
  changeStatus(ProcessID, 'p');
}

void processclass::resume(int ProcessID)
{
  if(checkForProgram(ProcessID) == -1)
  {
    Serial.println("terminated");
    return;
  }
  changeStatus(ProcessID, 'r');
}

void processclass::kill(int ProcessID)
{
  Serial.println(checkForProgram(ProcessID));
  if(checkForProgram(ProcessID) == -1)
  {
    Serial.println("terminated");
    return;
  }
  changeStatus(ProcessID, '0');
  memory.deleteMemory(ProcessID);
}

void processclass::changeStatus(int ProcessID, byte Status)
{
  if(ProgramTable[ProcessID].Status == Status)
  {
    Serial.println("already in state");
  }
  else
  {
    ProgramTable[ProcessID].Status = Status;  
    Serial.println("state changed");
  }
}

void processclass::listPrograms()
{
  Serial.println("All programs: ");
  for(int i = 0; i <10; i++)
  {
    if(ProgramTable[i].Status == 'r' || ProgramTable[i].Status == 'p')
    {
      Serial.println("Name: ");
      Serial.println(ProgramTable[0].Name);
      Serial.print("ID: ");
      Serial.println(ProgramTable[i].ProcessID);
      Serial.print("Status: ");
      Serial.println((char)ProgramTable[i].Status);
    }
  }
}

int processclass::checkForProgram(int ProcessID)
{
  for(int i = 0; i <10; i++)
  {
    if(ProgramTable[i].ProcessID == ProcessID)
    {
      if(ProgramTable[i].Status != 0)
      {
        return 1;
      }
      return -1;
    }
  }
  return -1;
}

processclass processes = processclass();
