#include "Arduino.h"
#include <EEPROM.h>
#include "fat.h"


fclass::fclass() {
}

//FAT
int FatUsage = 0;
EERef noOfFiles = EEPROM[160];
char readEEPROMarray[12];

int fclass::findFile(char Name[12])
{
  for (int i = 0; i < noOfFiles; i++)
  {
    fileEntry Entry;
    EEPROM.get(i * sizeof(fileEntry), Entry);
    if (strcmp(Name, Entry.Name) == 0) {
      return i;
    }
  }
  return -1;
}

void fclass::SETUP()
{
  noOfFiles = 0;
}

void readCharArrayEEPROM(int Location, int Size) {
  for (int i = 0; i < Size; i++) {
    EEPROM.get(Location, readEEPROMarray[i]);
    Location++;
  }
}

fileEntry fclass::retrieve(char Name[12])
{
  fatSort();
  for (int i = 0; i <= noOfFiles - 1; i++)
  {
    if (findFile(Name) != -1)
    {
      int file = findFile(Name);
      fileEntry Entry1;
      //int Data;
      EEPROM.get(file * sizeof(fileEntry), Entry1);
      //EEPROM.get(Entry1.Start, Data);
      readCharArrayEEPROM(Entry1.Start, Entry1.Length);
      Serial.println(readEEPROMarray);
      return Entry1;
    }
    else
    {
      Serial.println("F doesn't exist");
      return;
    }
  }
}

void fclass::fatSort()
{
  fileEntry Entry1;
  fileEntry Entry2;
  for (int i = 0; i < noOfFiles - 1; i++)
  {
    for (int j = 0; j < noOfFiles - i - 1; j++)
    {
      EEPROM.get(j * sizeof(fileEntry), Entry1);
      EEPROM.get(j + 1 * sizeof(fileEntry), Entry2);
      if (Entry1.Start > Entry2.Start)
      {
        fatSwap(Entry1, j, Entry2, j + 1);
      }
    }
  }
}

void fclass::fatSwap(fileEntry Entry1, int Pos1, fileEntry Entry2, int Pos2)
{
  EEPROM.put(Pos2 * sizeof(fileEntry), Entry1);
  EEPROM.put(Pos1 * sizeof(fileEntry), Entry2);
}

void fclass::erase(char Name[12])
{
  fatSort();
  for (int i = 0; i < noOfFiles - 1; i++)
  {
    if (findFile(Name) != -1)
    {
      int file = findFile(Name);
      noOfFiles--;
      fileEntry Entry1;
      EEPROM.get(noOfFiles * sizeof(fileEntry), Entry1);
      EEPROM.put((file) * sizeof(fileEntry), Entry1);
    }
    else
    {
      Serial.println("F doesn't exist");
      return;
    }
    fatSort();
  }
}




void fclass::files()
{
  Serial.println("All files: ");
  for (int i = 0; i < noOfFiles; i++)
  {
    fileEntry Entry;
    EEPROM.get(i * sizeof(fileEntry), Entry);
    Serial.print("Name: ");
    Serial.println(Entry.Name);
    Serial.print("Start: ");
    Serial.println(Entry.Start);
    Serial.print("Length: ");
    Serial.println(Entry.Length);
  }
}

void fclass::freespace()
{
  fatSort();
  int freeSpace = 0;
  int currentStart = 161;
  if (noOfFiles == 0)
  {
    Serial.println(1024 - 160);
    return;
  }
  for (int i = 0; i < noOfFiles - 1; i++)
  {
    fileEntry Entry1;
    EEPROM.get(i * sizeof(fileEntry), Entry1);
    fileEntry Entry2;
    EEPROM.get((i + 1) * sizeof(fileEntry), Entry2);
    if ((Entry1.Start + Entry1.Length - Entry2.Start) > freeSpace)
    {
      freeSpace = (Entry1.Start + Entry1.Length - Entry2.Start);
    }
  }
  fileEntry Entry;
  EEPROM.get((noOfFiles - 1) * sizeof(fileEntry), Entry);
  if ((1024 - 161 - (Entry.Start + Entry.Length)) > freeSpace)
  {
    Serial.println(Entry.Start + Entry.Length);
    freeSpace = 1024 - 161 - (Entry.Start + Entry.Length);
  }
  Serial.println(freeSpace);
}

void fclass::store(char Name[12], int Length, char data[])
{
  if (noOfFiles >= 10) {
    Serial.println("Not available");
    return;
  }
  for (int i = 0; i < noOfFiles; i++)
  {
    if (!findFile(Name) == -1) {
      Serial.println("already exists");
      return;
    }
  }
  if (noOfFiles == 0) {
    fileEntry newEntry =  {"", 161, Length};
    memcpy(newEntry.Name, Name, 11);
    //EEPROM.put(161, Data);
    writeCharArrayToEEPROM(data, 161, Length);
    EEPROM.put(0, newEntry);
    noOfFiles++;
    return;
  }
  if (noOfFiles == 1) {
    fileEntry Entry;
    EEPROM.get(0, Entry);
    fileEntry newEntry =  {"", (Entry.Start + Entry.Length), Length};
    memcpy(newEntry.Name, Name, 11);
    EEPROM.put(noOfFiles * sizeof(fileEntry), newEntry);
    //EEPROM.put((Entry.Start + Entry.Length), Data);
    writeCharArrayToEEPROM(data, Entry.Start + Entry.Length, Length);

    noOfFiles++;
    return;
  }
  else
  {
    fatSort();
    fileEntry Entry1;
    fileEntry Entry2;
    for (int i = 0; i < noOfFiles - 1; i++)
    {
      EEPROM.get(i * sizeof(fileEntry), Entry1);
      EEPROM.get(i + 1 * sizeof(fileEntry), Entry2);
      if ((Entry1.Start + Entry1.Length + Length) < Entry2.Start)
      {
        fileEntry newEntry =  {"", (Entry1.Start + Entry1.Length) + Length};
        memcpy(newEntry.Name, Name, 12);
        //EEPROM.put(Data, (Entry1.Start + Entry1.Length));
        writeCharArrayToEEPROM(data, Entry1.Start + Entry1.Length, Length);
        EEPROM.put((noOfFiles + 1)*sizeof(fileEntry), newEntry);
        noOfFiles++;
        return;
      }
    }
    fileEntry Entry;
    EEPROM.get((noOfFiles - 1)*sizeof(fileEntry), Entry);
    fileEntry newEntry =  {"", (Entry.Start + Entry.Length), Length};
    memcpy(newEntry.Name, Name, 11);
    EEPROM.put(noOfFiles * sizeof(fileEntry), newEntry);
    //EEPROM.put((Entry.Start + Entry.Length), Data);
    writeCharArrayToEEPROM(data, Entry.Start + Entry.Length, Length);
    noOfFiles++;
  }
}

void fclass::writeCharArrayToEEPROM(char input[], int locatie, int length) {
  int tempInt = locatie;
  for (int i = 0; i < length; i++) {
    //Serial.println(input[i]);
    if (input[i] == 0) {
      EEPROM.put(tempInt, 0);
      tempInt++;
    }
    else {
      EEPROM.put(tempInt, input[i]);
      tempInt++;
    }
  }
}

fclass fat = fclass();
