#include "Arduino.h"
#include "processes.h"
#include "fat.h"
#include "memory.h"
#include "stack.h"
#include "instructions.h"
#include <EEPROM.h>


instructionclass::instructionclass() {
}

void instructionclass::runProcesses() {
  for (int i = 0; i < 10; i++) {
    //Serial.println(ProgramTable[i].Name);
    if (ProgramTable[i].Status == 'r') {
      execute(i);
      //Serial.println(i);

    }
  }
}

void instructionclass::execute(int indexProces) {
  //unsigned char tempOpslaan;
  int temp;
  unsigned char tempChar;
  //signed int test;
  int a1;
  int a2;
  //for every FAT entry
  for (int i = 0; i < 160; i += 16) {
    fileEntry Entry;
    EEPROM.get(i, Entry);
    //check if running
    if (strcmp(Entry.Name, ProgramTable[indexProces].Name) == 0) {
      EEPROM.get(ProgramTable[indexProces].ProgramCounter++, tempChar);

      Serial.print(F("ProgramCounter:"));
      Serial.println(ProgramTable[indexProces].ProgramCounter);
      //temp = atoi(tempOpslaan);
      temp = tempChar;
      //Serial.println(test);
      //delay(100);
      break;
    }
  }

  switch (temp) {
    case STOP:
      Serial.print(F("STOP:  ") );
      processes.kill(indexProces);
      break;
    case ENDIF:
      Serial.println(F("ENDIF") );
      stack.popByte(indexProces);
      stack.popInt(indexProces);
      break;
    case EQUALS:
      Serial.println(F("EQUALS") );
      stack.popByte(indexProces);
      a1 = stack.popInt(indexProces);
      stack.popByte(indexProces);
      a2 = stack.popInt(indexProces);
      Serial.print(F("VALUE 1: ") );
      Serial.println(a1);
      Serial.print(F("VALUE 2: ") );
      Serial.println(a2);
      if (a1 == a2)
      {
        stack.pushInt(1, indexProces);
      }
      else
      {
        stack.pushInt(0, indexProces);
      }
      break;
    case IF:
      Serial.println(F("IF") );
      stack.popByte(indexProces);
      a1 = stack.popInt(indexProces);
      Serial.print(F("TRUE OR FALSE? ") );
      Serial.println(a1);
      EEPROM.get(ProgramTable[indexProces].ProgramCounter++, tempChar);
      Serial.print(F("ADD TO PC:  ") );
      Serial.println(tempChar);
      if (a1 == 1)
      {
        break;
      }
      else
      {
        ProgramTable[indexProces].ProgramCounter += tempChar;
        stack.pushInt(a1, indexProces);
        Serial.print(F("PC IS NOW:  ") );
        Serial.println(ProgramTable[indexProces].ProgramCounter);
      }
      break;
    case ELSE:
      Serial.println(F("ELSE") );
      stack.popByte(indexProces);
      a1 = stack.popInt(indexProces);
      Serial.print(F("TRUE OR FALSE? ") );
      Serial.println(a1);
      EEPROM.get(ProgramTable[indexProces].ProgramCounter++, tempChar);
      if (a1 == 0)
      {
        break;
      }
      else
      {
        ProgramTable[indexProces].ProgramCounter += tempChar;
        stack.pushInt(a1, indexProces);
        Serial.print(F("PC IS NOW:  ") );
        Serial.println(ProgramTable[indexProces].ProgramCounter);
      }
      break;
    case LOOP:
      ProgramTable[indexProces].LoopStart = ProgramTable[indexProces].ProgramCounter - 1;
      //Serial.println(F("Loop Start: "));
      //Serial.println(ProgramTable[indexProces].LoopStart);
      break;
    case ENDLOOP:
      //Serial.println(F("PC: "));
      ProgramTable[indexProces].ProgramCounter = ProgramTable[indexProces].LoopStart;

      //Serial.println(ProgramTable[indexProces].ProgramCounter);
      break;
    case MILLIS:
      temp = millis();
      stack.pushInt(temp, indexProces);

      break;
    case DELAYUNTIL:
      //Serial.println(F("DELAY"));
      //int wait = stack.popInt(indexProces);
      stack.popByte(indexProces);
      temp = stack.popInt(indexProces);
      //Serial.println(millis());
      //Serial.println(temp);
      if (millis() > temp)
      {

      }
      else
      {
        ProgramTable[indexProces].ProgramCounter--;
        stack.pushInt(temp, indexProces);
        //ProgramTable[indexProces].ProgramCounter--;
      }
      break;
    case PLUS:

      //Serial.println(F("PLUS: "));
      //Serial.println(F("int__"));
      stack.popByte(indexProces);
      a1 = stack.popInt(indexProces);
      stack.popByte(indexProces);
      a2 = stack.popInt(indexProces);
      a1 = a1 + a2;
      //Serial.println(a1);
      //Serial.println("PLUS");
      stack.pushInt(a1, indexProces);
      break;

    case PRINT:
      //Serial.println(F("PRINT"));
      tempChar = stack.popByte(indexProces);
      if (tempChar == STRING)
      {
        //Serial.println("Print");
        int t = stack.popByte(indexProces);
        char result[t];
        for (int i = t; i >= 0; i--)
        {
          //Serial.println(i);
          result[i] = stack.popByte(indexProces);
        }
        Serial.print(result);
      }
      if (tempChar == INT)
      {

      }
      break;
    case PRINTLN:
      //Serial.println(F("PRINTLN"));
      tempChar = stack.popByte(indexProces);
      if (tempChar == STRING)
      {
        temp = stack.popByte(indexProces);
        char result[temp];
        for (int i = temp; i >= 0; i--)
        {
          //Serial.println(char(i));
          result[i] = stack.popByte(indexProces);
        }
        Serial.println(result);
      }
      if (tempChar == INT)
      {
        a1 = stack.popByte(indexProces);
        a2 = stack.popByte(indexProces);
        Serial.println(a1 + (256 * a2));
      }
      if (tempChar == FLOAT)
      {
        //float f = stack.popFloat(indexProces);
        Serial.println(stack.popFloat(indexProces));
      }
      if (tempChar == CHAR)
      {
        //char c = stack.popByte(indexProces);
        Serial.println(char(stack.popByte(indexProces)));
      }

      break;
    case INCREMENT:
      tempChar = stack.popByte(indexProces);
      if (tempChar == CHAR)
      {
        char c = stack.popByte(indexProces);
        c++;
        stack.pushByte(c, indexProces);
        stack.pushByte(CHAR, indexProces);
      }
      if (tempChar == INT)
      {
       //int a1 = stack.popByte(indexProces);
       //int a2 = stack.popByte(indexProces);
        a1 = stack.popInt(indexProces);
        a1++;
        //Serial.print("inc: " );
        //Serial.println(a1);
        stack.pushInt(a1, indexProces);
        //a1 = a1 + (256 * a2));
        //stack.pushByte(a2, indexProces);
        //stack.pushByte(a1, indexProces);
        //stack.pushByte(INT, indexProces);
      }
      if (tempChar == FLOAT)
      {
        float f = stack.popFloat(indexProces);
        f++;
        stack.pushFloat(f, indexProces);
      }
      break;
    case SET: //should work fine
      Serial.println(F("SET!"));
      //unsigned char varName;
      EEPROM.get(ProgramTable[indexProces].ProgramCounter++, tempChar);
      //Serial.println((char)varName);
      memory.reserve(tempChar, indexProces);
      break;
    case GET: //needs to implement different types
      //Serial.println(F("GET!"));
      //unsigned char a;
      EEPROM.get(ProgramTable[indexProces].ProgramCounter++, tempChar);
      //Serial.println((char)a);
      memory.readMemory(tempChar, indexProces);
      break;
    case INT:
      //Serial.println(F("INT!"));
      for (int i = 0; i < 2; i++) {
        EEPROM.get(ProgramTable[indexProces].ProgramCounter++, tempChar);
        temp = tempChar;
        stack.pushByte(temp, indexProces);
        //Serial.println(tempint);
      }
      stack.pushByte(INT, indexProces);
      break;

    case FLOAT:
      //Serial.println(F("FLOAT!"));
      for (int i = 0; i < 4; i++) {
        EEPROM.get(ProgramTable[indexProces].ProgramCounter++, tempChar);
        int tempint = tempChar;
        stack.pushByte(tempint, indexProces);
        //Serial.println(i);
      }
      //Serial.println(F("FLOAT!"));
      stack.pushByte(FLOAT, indexProces);
      break;

    case CHAR:
      //unsigned char a;
      EEPROM.get(ProgramTable[indexProces].ProgramCounter++, tempChar);
      stack.pushByte(tempChar, indexProces);
      //Serial.println(F("Char!"));
      //Serial.println(char(a));
      stack.pushByte(CHAR, indexProces);
      break;
    case STRING:
      //unsigned char a;
      int iteraties = 0;
      while (true) {
        EEPROM.get(ProgramTable[indexProces].ProgramCounter++, tempChar);
        stack.pushByte(tempChar, indexProces);
        //command = a;
        //Serial.println(a);
        //Serial.println((char)command);
        if (tempChar == 0) {
          stack.pushByte(iteraties, indexProces);
          stack.pushByte(STRING, indexProces);
          break;
        }

        iteraties++;
      }
      break;
  }
}

instructionclass instructions = instructionclass();
