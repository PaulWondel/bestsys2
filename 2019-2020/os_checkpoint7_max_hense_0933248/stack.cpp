#include "Arduino.h"
#include "processes.h"
#include "stack.h"
#include "memory.h"


stackclass::stackclass(){}
char popStringArray[12] = "";

void stackclass::SETUP()
{
}

void stackclass::pushByte(byte b, int i) {
  ProgramTable[i].stack_[ProgramTable[i].StackPointer++] = b;
}

byte stackclass::popByte(int i) {
  return ProgramTable[i].stack_[--ProgramTable[i].StackPointer];
}

byte stackclass::Peek(int proces) {
  byte temp = popByte(proces);
  pushByte(temp, proces);
  return temp;
}

float stackclass::popVal(int proces) {
  byte temp = popByte(proces);
  if (temp == INT) {
    return popInt(proces);
  }
  if (temp == FLOAT) {
    return popFloat(proces);
  }

  if (temp == CHAR) {
    return popByte(proces);
  }
}

float stackclass::popFloat(int proces) {
  byte b[4];
  float *pf = (float *)b;

  for (int i = 0; i < 4; i++) {
    b[i] = popByte(proces);
  }
  return *pf;
}

int stackclass::popInt(int proces) {
  word LowByte = popByte(proces);
  word HighByte = popByte(proces);
  return LowByte + (HighByte * 256);
}

void stackclass::popString(int proces) {
  int amoutChar = popByte(proces);
  for (int i = amoutChar - 1; i > -1; i--) {
    popStringArray[i] = popByte(proces); // popStringArray is een globale variable
  }
}

void stackclass::pushString(char string[12], int proces) {
  for (int i = 0; i < 12; i++) {
    if (string[i] == '\0') {
      pushByte(i, proces);
      pushByte(STRING, proces);//Moet je eruit halen bij het testen!
      return;
    }
    pushByte(string[i], proces);
  }
}

void stackclass::pushFloat(float kommaGetal, int proces) {
  byte b[4];
  float *pf = (float *)b;
  *pf = kommaGetal;

  for (int i = 3; i > -1; i--) {
    //Serial.println(b[i]);
    pushByte(b[i], proces);
  }
  pushByte(FLOAT, proces); //Moet je weg laten voor testen
}

void stackclass::pushInt(int getal, int proces) {
  pushByte(highByte(getal), proces);
  pushByte(lowByte(getal), proces);
  pushByte(INT, proces); // moet je weghalen voor testen
}


stackclass stack = stackclass();
