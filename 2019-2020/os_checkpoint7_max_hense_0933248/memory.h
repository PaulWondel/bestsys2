#ifndef memory_h
#define memory_h
#include "Arduino.h"

class memclass
{
  public:
    memclass();
    void SETUP();
    void reserve(byte Name, int ProcessId);
    void getTypeOfStack(int proces);
    byte popByte();
    void pushByte(byte b);
    void checkInMemoryTabel();
    void memoryWrite(int beginPosition, int amount, int ProcessId);
    void setSize();
    void deleteMemory(int ProcessID);
    void readMemory (byte Name, int ProcessID);
    void visualizeMemoryGeheugen();
    void visualizeMemoryTabel();
};

extern memclass memory;
#endif
