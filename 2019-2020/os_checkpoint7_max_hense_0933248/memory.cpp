#include "Arduino.h"
#include "memory.h"
#include "instruction_set.h"
#include "stack.h"


byte memoryStore[256];
int noOfVars;

int beginArray;
int beginPosition;
int amountBytes;
int tempType;

typedef struct {
  byte Name;
  int Type;
  int Size;
  int Address;
  int ProcessID;
} memoryEntry;

static memoryEntry memoryTable[25];

memclass::memclass() {
}

void memclass::reserve(byte Name, int ProcessId) {
  // check if there is enough space
  //Serial.print(F("name "));
  //Serial.println(Name);
  if (noOfVars >= 25) {
    Serial.println("no available space!");
    return;
  }
  // Check if the variable and process id combination already exists
  for (int i = 0; i < noOfVars - 1; i++)
  {
    if (memoryTable[i].Name == Name ) {
      if (memoryTable[i].ProcessID == ProcessId ) {
        //Serial.println(F("V already exists"));
        // shift all remaining memory entries 1 down
        for (int j = i; j < 24; j++) {
          memoryTable[j] = memoryTable[j + 1];
        }
        noOfVars--;
        break;
      }
    }
  }
  //Serial.println(F("-----VAR------"));
  getTypeOfStack(ProcessId);
  checkInMemoryTabel();

  memoryWrite(beginPosition, amountBytes, ProcessId);
//Serial.print(F("beginPosition "));
  //Serial.println(beginPosition);
  //Serial.println(beginArray);
  
  memoryTable[beginArray].Name = Name;
  memoryTable[beginArray].Type = tempType;
  memoryTable[beginArray].Address = beginPosition;
  memoryTable[beginArray].Size = amountBytes;
  memoryTable[beginArray].ProcessID = ProcessId;
  if (memoryTable[beginArray + 1].Name == 0) {
    memoryTable[beginArray + 1].Address = beginPosition + amountBytes;
  }
  // investigate
  setSize();
  //Serial.println(char(memoryTable[beginArray].Name));
  //Serial.println(memoryTable[beginArray].Type);
  //Serial.println(memoryTable[beginArray].Size);
  //Serial.println(F("-----END-VAR------"));
}

void memclass::setSize() {
  memoryTable[24].Name = 'a';
  memoryTable[24].ProcessID = 100;
  memoryTable[24].Size = 0;
  memoryTable[24].Address = 255;
}

void memclass::deleteMemory(int ProcessID) {
  for (int i = 0; i < 25; i++) {
    if (memoryTable[i].ProcessID == ProcessID) {
      noOfVars--;
      memoryTable[i].Name = 0;
      memoryTable[i].Size = 0;
      memoryTable[i].ProcessID = 0;
      if (memoryTable[i].Address != 0 and memoryTable[i - 1].Name != 0) {
        memoryTable[i].Address = memoryTable[i - 1].Address + memoryTable[i - 1].Size;
      }
      else {
        memoryTable[i].Address = 0;
      }
    }
  }
}


void memclass::readMemory (byte Name, int ProcessID) {
  for (int i = 0; i < 25; i++) {
    if (memoryTable[i].Name == Name and memoryTable[i].ProcessID == ProcessID) {

      for (int a = 0 ; a < memoryTable[i].Size; a++) {
        //Serial.println(memoryStore[memoryTable[i].Address + a]);
        stack.pushByte(memoryStore[memoryTable[i].Address + a], ProcessID);
      }
      if (memoryTable[i].Type == STRING) {
        stack.pushByte(memoryTable[i].Size, ProcessID);
      }
      stack.pushByte(memoryTable[i].Type, ProcessID);
      return;
    }
  }
}

void memclass::checkInMemoryTabel() {
  //Serial.println(amountBytes);
  for (int i = 0; i < 25; i++) {
    if (memoryTable[i].Name == 0 and memoryTable[i].ProcessID == 0) {
      int firstAddress = memoryTable[i].Address;
       
      for (int j = i + 1; j < 25; j++) {
        if (memoryTable[j].Name != 0 and memoryTable[j].ProcessID != 0) {
          int lastAddress = memoryTable[j].Address;
          //Serial.print((lastAddress - firstAddress));
          //Serial.print(">");
          //Serial.println(amountBytes);
          if ((lastAddress - firstAddress) > amountBytes) {
            //Serial.print("Free space at: ");
            //Serial.print(i);
            //Serial.print(" Size = ");
            //Serial.println((lastAddress - firstAddress)); 
            beginArray = i;
            beginPosition = firstAddress;
            return;
          }
        }
      }
    }
  }
}

void memclass::memoryWrite(int beginPosition, int amount, int ProcessId) {
  //Serial.println(F("writing!!"));
  for (int i = beginPosition + amount; i > beginPosition; i--) {
    byte temp = stack.popByte(ProcessId);
    memoryStore[i - 1] = temp;
  }
  noOfVars++;
}

//Retrieve what Type is on stack
void memclass::getTypeOfStack(int ProcessId) {
  tempType = stack.popByte(ProcessId);
  //Serial.println(tempType);
  if (tempType == INT) {
     amountBytes = 2;
  }
  if (tempType == CHAR) {
     amountBytes = 1;
  }
  if (tempType == FLOAT) {
     amountBytes = 4;
  }
  if (tempType == STRING) {
    amountBytes = stack.popByte(ProcessId);
  }
}

void memclass::pushByte(byte b) {
 stack_test[sp++] = b;
}

byte memclass::popByte() {
return stack_test[--sp];
}

void memclass::SETUP() {
  
}

void memclass::visualizeMemoryTabel() {
  for (int i = 0; i < 25; i++) {
    Serial.print(char(memoryTable[i].Name)); Serial.print(" "); Serial.print(memoryTable[i].ProcessID); Serial.print(" "); Serial.print(memoryTable[i].Size); Serial.print(" "); Serial.print(memoryTable[i].Address); Serial.println();
  }
}

void memclass::visualizeMemoryGeheugen() {
  for (int i = 0; i < 25; i++) {
    Serial.println(memoryStore[i]);
  }
}

memclass memory = memclass();
