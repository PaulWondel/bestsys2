#ifndef EXTRA_H
#define EXTRA_H

const int BUFSIZE = 12; // Size of the buffer
static int index = 0; // Index number for selecting a function
char receivedChars[BUFSIZE];   // an array to store the received data

const int nameSize = 12;
const int parameterSize= 10;
const int descriptSize= 30;

void callFunction();
void buffer();
void printCommandList();
void store();
void retrieve();
void erase();
void files();
void freespace();
void run();
void list();
void suspend();
void resume();
void kill();

// Defines a function using a struct
typedef struct {
  char name [BUFSIZE];
  void *func;
} commandType;

typedef struct{
  char name [nameSize];
  char parameter [parameterSize];
  char descr[descriptSize];
} commandList;

// Places a command in a array
static commandType command[] = {
  {"store", &store},
  {"retrieve", &retrieve},
  {"erase", &erase},
  {"files", &files},
  {"freespace", &freespace},
  {"run", &run},
  {"list", &list},
  {"suspend", &suspend},
  {"resume", &resume},
  {"kill", &kill}
};

static commandList infoList[] = {
  {"store", "file, size, data", "Store files in filesystem"},
  {"retrieve", "file", "Retrieve file from filesystem"},
  {"erase", "file", "Erase file from filesystem"},
  {"files", "", "Lists files present in filesystem"},
  {"freespace", "", "Free space in filesystem"},
  {"run", "", "Run program"},
  {"list", "", "List running processes"},
  {"suspend", "id", "Suspend process"},
  {"resume", "id", "Resume process"},
  {"kill", "id", "Kill process"}
};
#endif