#include "extra.h"

// Length of the array
static int commandSize = sizeof(command) / sizeof(commandType);
static int commandListSize = sizeof(infoList) / sizeof(commandList);

bool existingCommand = false; // Check tate if the command exists
bool newData = false; // Check state if there is new data

void setup() {
    Serial.begin(9600);
    Serial.println("<Welcome to Arduino OS!>");
}

void loop() {
    buffer();
    callFunction();
}

// To call the function from the array
void callFunction(){
  if(newData){
    for (int i = 0; i < commandSize; i++)
    {
      if (strcmp(receivedChars, command[i].name) == 0)
      {
        void (*func)() = command[i].func;
        func();
        existingCommand = true;
        break;
      }
    }
    if(existingCommand)
    {
      newData = false;
      existingCommand = false;
      return;
    }
    Serial.print((String)"Command '"+receivedChars+"' unknown.");
    Serial.println(" Use commands from the following list: ");
    printCommandList();
    newData = false;
  }
}

// Serial buffer from the input
void buffer() {
  char answer;
 
  while (Serial.available() > 0 && newData == false)
  {
    answer = Serial.read();
    if (answer != '\n') 
    {
      receivedChars[index] = answer;
      index++;
      if (index >= BUFSIZE) 
      {
        index = BUFSIZE - 1;
      }
      return;
    }
    receivedChars[index] = '\0';
    index = 0;
    newData = true;
  }
}

// Print Contents of the Buffer
void printInputData() {
    if (newData) 
    {
        Serial.print("Buffer: ");
        Serial.println(receivedChars);
        newData = false;
    }
}

// Print the list of commands with description
void printCommandList(){
  char commandName;
  for (int i = 0; i < commandListSize; i++)
  {
    Serial.print(infoList[i].name);
    Serial.print(" - ");
    Serial.println(infoList[i].descr);
  }
  Serial.print("\n");
}

// Functions
void store(){
  Serial.println("This is the store function");
}

void retrieve(){
  Serial.println("This is the retrieve function");
}

void erase(){
  Serial.println("This is the erase function");
}

void files(){
  Serial.println("This is the files function");
}

void freespace(){
  Serial.println("This is the freespace function");
}

void run(){
  Serial.println("This is the run function");
}

void list(){
  Serial.println("This is the list function");
}

void suspend(){
  Serial.println("This is the suspend function");
}

void resume(){
  Serial.println("This is the resume function");
}

void kill(){
  Serial.println("This is the kill function");
}