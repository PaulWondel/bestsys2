#include <Arduino.h>

const byte BUFSIZE = 30;
const byte numChars = 32;
char receivedChars[numChars];   // an array to store the received data

boolean newData = false;

void setup() {
    Serial.begin(9600);
    Serial.println("<Arduino is ready>");
}

void loop() {
    recvWithEndMarker();
    showNewData();
}

// Defineerd een functie d.m.v. een struct
typedef struct {
  char name [BUFSIZE];
  void *func;
} commandType;

// Plaatst commando in een array
static commandType command[] = {
  {"store", &store},
  {"retrieve", &retrieve},
  {"erase", &erase},
  {"files", &files},
  {"freespace", &freespace},
  {"run", &run},
  {"list", &list},
  {"suspend", &suspend},
  {"resume", &resume},
  {"kill", &kill}
};

void store(){
  Serial.println("This is the store function");
}

void retrieve(){
  Serial.println("This is the retrieve function");
}

void erase(){
  Serial.println("This is the erase function");
}

void files(){
  Serial.println("This is the files function");
}

void freespace(){
  Serial.println("This is the freespace function");
}

void run(){
  Serial.println("This is the run function");
}

void list(){
  Serial.println("This is the list function");
}

void suspend(){
  Serial.println("This is the suspend function");
}

void resume(){
  Serial.println("This is the resume function");
}

void kill(){
  Serial.println("This is the kill function");
}

// Lengte van array
static int n = sizeof(command) / sizeof(commandType);

// Om functie uit de array aan te roepen
void callFunction(char select){
  void (*func)() = command[select].func;
  func();
}

// Example 2 - Receive with an end-marker

void recvWithEndMarker() {
    static byte ndx = 0;
    char endMarker = '\n';
    char rc;
   
    while (Serial.available() > 0 && newData == false) {
        rc = Serial.read();

        if (rc != endMarker) {
            receivedChars[ndx] = rc;
            ndx++;
            if (ndx >= numChars) {
                ndx = numChars - 1;
            }
        }
        else {
            receivedChars[ndx] = '\0'; // terminate the string
            ndx = 0;
            newData = true;
        }
    }
}

void showNewData() {
    if (newData == true) {
        Serial.print("This just in ... ");
        Serial.println(receivedChars);
        newData = false;
    }
}
