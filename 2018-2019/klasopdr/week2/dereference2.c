#include <stdio.h>

int main(void)
{
	//normal variable
	int num = 100;      

	//pointer variable
	int *ptr;           

	//pointer initialization
	ptr = &num;         

	//printing the value
	printf("value of num = %d\n", *ptr);

	//printing the addresses
	printf("Address of num: %x\n", &num);
	printf("Address of ptr: %x\n", &ptr);

	return 0;
}