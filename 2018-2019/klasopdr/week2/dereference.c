// A simple C program to introduce dereference operator 
#include<stdio.h> 
#include<stdlib.h> 

struct MY_STRUCT 
{
    int my_int;
    float my_float;
};
//a pointer to an instance of this struct 
struct MY_STRUCT *instance;

int main() 
{  
	
    struct MY_STRUCT info = {1, 3.141593};
    instance = &info;
    //dereference it to access its members using one of two different notations
    int a = (*instance).my_int;
    float b = instance->my_float;
    printf("The value of a = %d\n", a);
    printf("The value of b = %f\n", b);

return 0; 
} 

