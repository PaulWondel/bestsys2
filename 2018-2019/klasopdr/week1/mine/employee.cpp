#include <stdio.h>
#include <string.h>

struct Employee {
	char name[50];		
	char geslacht[10];
	int leeftijd;
	int werknummer;
};

void printEmployee(struct Employee *employee){
	printf("Werker Naam: %s\n",employee->name);
	printf("Werker Geslacht: %s\n",employee->geslacht);
	printf("Werker Leeftijd: %i\n",employee->leeftijd);
	printf("Werker Werknummer: %i\n",employee->werknummer);
}

/*
void createWerker(struct werker, string x, string y, int a, int b){
}
*/

int main(){

	struct Employee Werker1;
	struct Employee Werker2;

	// Employee Specificatie
	strcpy(Werker1.name, "John Baptiste");
	strcpy(Werker1.geslacht, "Man");
	Werker1.leeftijd = 25;
	Werker1.werknummer = 4974723;

	printEmployee(&Werker1);

	return 0;
}
