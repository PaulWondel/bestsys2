#include <stdio.h>

void swap(int *a, int *b){
	int temp;
	temp = *a;
	*a=*b;
	*b=temp;
}


int main(){

	int a = 10;
	int b = 25;

	//Values
	printf("Value a=%d \n",a);
	printf("Value b=%d \n",b);
	
	//Addresses
	printf("Address a=%d \n",&a);
	printf("Address b=%d \n",&b);

	printf("\n");

	swap(&a,&b);

	printf("Value a=%d \n",a);
	printf("Value b=%d \n",b);

	printf("Address a=%d \n",&a);
	printf("Address b=%d \n",&b);
	
	return 0;

}
