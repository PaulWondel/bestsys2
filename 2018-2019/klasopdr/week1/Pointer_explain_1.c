//https://www.youtube.com/watch?v=TFTpRjPcoUs
#include <stdio.h>
#include <stdlib.h>

int main()
{
    int a=20;
    int b =10;
    printf("The address of a is %p:\n",&a);
    printf("The address of b is %p:\n",&b);
    int *p1 = &a;
    printf("The address of a is %p:\n",p1);
    printf(" a is %d:\n", *p1);
    int *p2 = &b;
    printf("The address of b is %p:\n",p2);
    printf(" b is %d:\n", *p2);
    p1 = p2;
    printf("The address of b is %p:\n",p1);
    printf(" b is %d:\n", *p1);
    printf("the sum of a and b is %d:\n", *p1+ *p2);
    return 0;
}



/*
int b =10;
    printf("The address of a is %u:\n",&a);

    int *p1 = &a;
    int *p2 = &b;
    printf("The address of a is %u:\n",p1);
    printf(" a is %d:\n", *p1);
    printf("The address of b is %u:\n",p2);
    printf("b is %d:\n", *p2);
    printf("the sum of a and b is %d:\n", *p1+ *p2);
    p1 = p2;
    printf("The address of b is %u:\n",p1);
    printf("b is %d:\n", *p1);
*/
