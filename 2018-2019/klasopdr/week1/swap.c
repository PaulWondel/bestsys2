#include <stdio.h>
void swap (int *a, int *b);
int main() {
    int m = 25;
    int n = 100;
    printf("m = %d, n = %d\n", m, n);
    printf("Address of m = %p\n",&m);
    printf("Address of n = %p\n",&n);
    swap(&m, &n);
    printf("m = %d, n = %d\n", m, n);
    printf("Address of m = %p\n",&m);
    printf("Address of n = %p\n",&n);
    return 0;
}
void swap (int *a, int *b) {
    int temp;
    temp = *a;
    *a = *b;
    *b = temp;
}
